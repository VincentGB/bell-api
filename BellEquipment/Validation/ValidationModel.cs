﻿using BellEquipment.Common;
using System;

namespace BellEquipment.Models
{
    public class ValidationModel
    {
        public static bool ValidReportData(
            DateTime? ReportDate,
            int? ReportPersonId,
            int? DealersId,
            string Customer,
            string VinNo,
            int? ModelId,
            int? ReportTypeId,
            int? ReportedToId,
            int? FaultCodeId,
            string NonStandardAdditions,
            string FaultCodeReported,
            string FailedPartNumber,
            string ReplacementPartNumber,
            int? ComplaintId,
            int? DefectId,
            string ComplaintDescription,
            string FailedComponentSN,
            string RootCause,
            string ActionRequired,
            bool? SafetyHard,
            int? ApplicaitonId,
            decimal? Hours)
        {
            // TODO What is the minimum requirements for a Report?
            if (
                !string.IsNullOrEmpty(VinNo)  && !string.IsNullOrEmpty(FaultCodeId.ToString())
                //!string.IsNullOrEmpty(ReportPersonId.ToString()) && !string.IsNullOrEmpty(FaultCodeReported)
                //&& !string.IsNullOrEmpty(DealersId.ToString()) && !string.IsNullOrEmpty(Customer)
                //&& !string.IsNullOrEmpty(ModelId.ToString()) && !string.IsNullOrEmpty(ReportTypeId.ToString()) && !string.IsNullOrEmpty(ReportedToId.ToString()) 
                //&& !string.IsNullOrEmpty(NonStandardAdditions) && !string.IsNullOrEmpty(FailedPartNumber) && !string.IsNullOrEmpty(ReplacementPartNumber)
                //&& !string.IsNullOrEmpty(ComplaintId.ToString()) && !string.IsNullOrEmpty(DefectId.ToString()) && !string.IsNullOrEmpty(ComplaintDescription)
                //&& !string.IsNullOrEmpty(RootCause) && !string.IsNullOrEmpty(ActionRequired) && !string.IsNullOrEmpty(SafetyHard)
                //&& !string.IsNullOrEmpty(ApplicaitonId.ToString()) 
                && Utils.CheckIsDecimal(Hours.ToString())
                )
            {
                // Numeric Invalid Check
                if (ReportPersonId != null && !Utils.CheckIsNumeric(ReportPersonId.ToString()))
                    return false;
                if (DealersId != null && !Utils.CheckIsNumeric(DealersId.ToString()))
                    return false;
                if (ModelId != null && !Utils.CheckIsNumeric(ModelId.ToString()))
                    return false;
                if (ReportTypeId != null && !Utils.CheckIsNumeric(ReportTypeId.ToString()))
                    return false;
                if (ReportedToId != null && !Utils.CheckIsNumeric(ReportedToId.ToString()))
                    return false;
                if (FaultCodeId != null && !Utils.CheckIsNumeric(FaultCodeId.ToString()))
                    return false;
                if (ComplaintId != null && !Utils.CheckIsNumeric(ComplaintId.ToString()))
                    return false;
                if (DefectId != null && !Utils.CheckIsNumeric(DefectId.ToString()))
                    return false;
                if (ApplicaitonId != null && !Utils.CheckIsNumeric(ApplicaitonId.ToString()))
                    return false;
                if (Hours != null && !Utils.CheckIsNumeric(Hours.ToString()))
                    return false;
                return true;
            }
            // TODO Should this function return a list of invalid fields?
            return false;
        }

        public static bool ValidReportDataObject(Models.BellDbObject.Report report)
        {
            // TODO What is the minimum requirements for a Report?
            if (
                !string.IsNullOrEmpty(report.VinNo) && !string.IsNullOrEmpty(report.FaultCodeId.ToString()) && !string.IsNullOrEmpty(report.FaultSubCodeId.ToString())
                //!string.IsNullOrEmpty(ReportPersonId.ToString()) && !string.IsNullOrEmpty(FaultCodeReported)
                //&& !string.IsNullOrEmpty(DealersId.ToString()) && !string.IsNullOrEmpty(Customer)
                //&& !string.IsNullOrEmpty(ModelId.ToString()) && !string.IsNullOrEmpty(ReportTypeId.ToString()) && !string.IsNullOrEmpty(ReportedToId.ToString()) 
                //&& !string.IsNullOrEmpty(NonStandardAdditions) && !string.IsNullOrEmpty(FailedPartNumber) && !string.IsNullOrEmpty(ReplacementPartNumber)
                //&& !string.IsNullOrEmpty(ComplaintId.ToString()) && !string.IsNullOrEmpty(DefectId.ToString()) && !string.IsNullOrEmpty(ComplaintDescription)
                //&& !string.IsNullOrEmpty(RootCause) && !string.IsNullOrEmpty(ActionRequired) && !string.IsNullOrEmpty(SafetyHard)
                //&& !string.IsNullOrEmpty(ApplicaitonId.ToString()) 
                && Utils.CheckIsDecimal(report.Hours.ToString())
                )
            {
                // Numeric Invalid Check
                if (report.DealersId != null && !Utils.CheckIsNumeric(report.DealersId.ToString()))
                    return false;
                if (report.ModelId != null && !Utils.CheckIsNumeric(report.ModelId.ToString()))
                    return false;
                if (report.ReportTypeId != null && !Utils.CheckIsNumeric(report.ReportTypeId.ToString()))
                    return false;
                if (report.FaultCodeId != null && !Utils.CheckIsNumeric(report.FaultCodeId.ToString()))
                    return false;
                if (report.ComplaintId != null && !Utils.CheckIsNumeric(report.ComplaintId.ToString()))
                    return false;
                if (report.DefectId != null && !Utils.CheckIsNumeric(report.DefectId.ToString()))
                    return false;
                if (report.ApplicaitonId != null && !Utils.CheckIsNumeric(report.ApplicaitonId.ToString()))
                    return false;
                if (report.Hours != null && !Utils.CheckIsDecimal(report.Hours.ToString()))
                    return false;
                return true;
            }
            // TODO Should this function return a list of invalid fields?
            return false;
        }

    }

}