﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BellEquipment.Common
{
    public class ErrorHandler
    {
        public enum ValidationError
        {
            [Description("Unable to authenticate")]
            Authentication,
            [Description("Username or authentication invalid")]
            AuthenticationUpdatePassword,
            [Description("Unauthorised request")]
            UnAuthorized,
            [Description("Invalid request parameters")]
            InvalidParameters,
            [Description("Unable to save record")]
            DatabaseRecordError,
            [Description("Functionality does not exist")]
            FunctionalityDoesNotExist
        }
    }
}