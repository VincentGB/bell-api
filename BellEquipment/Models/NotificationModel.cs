﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using static BellEquipment.Models.BellDbObject;

namespace BellEquipment.Models
{
    public class NotificationModel
    {
        static string connString = System.Configuration.ConfigurationManager.ConnectionStrings["bellDBConnectionString"].ConnectionString.ToString();

        public enum NotificationTypes
        {
            [Description("ReportCreated")]
            ReportCreated,
            [Description("ReportUpdated")]
            ReportUpdated,
            [Description("ReportClosed")]
            ReportClosed,
            [Description("Message")]
            ReportMessage
        }

        public static async Task NotificationCreateAsync(string ReportId, string ActionType, string ActionId, string UserId)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        BellDbObject.Notification n = new BellDbObject.Notification()
                        {
                            ReportId = Convert.ToInt32(ReportId),
                            ActionType = ActionType,
                            ActionId = Convert.ToInt32(ActionId),
                            CreatedBy = UserId,
                            CreatedDate = DateTime.Now
                        };
                        dc.Notification.InsertOnSubmit(n);
                        try
                        {
                            dc.SubmitChanges();
                            // TODO
                            // Fire Push notification about new notification
                        }
                        catch (Exception ex) { }
                    }
                }
            }
            catch (Exception e) { }
        }

        public static int CreateNotificationAsync(string ReportId, string ActionType, string ActionId, string UserId)
        {
            PushNotification notification = new PushNotification();
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        BellDbObject.Notification n = new BellDbObject.Notification()
                        {
                            ReportId = Convert.ToInt32(ReportId),
                            ActionType = ActionType,
                            ActionId = Convert.ToInt32(ActionId),
                            CreatedBy = UserId,
                            CreatedDate = DateTime.Now
                        };
                        dc.Notification.InsertOnSubmit(n);
                        try
                        {
                            dc.SubmitChanges();
                            // TODO
                            // Fire Push notification about new notification
                            return n.ID;
                        }
                        catch (Exception ex) {
                            return 0;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public static dbResult Notifications()
        {
            dbResult result = new dbResult() { result = new BellDbObject.Notification() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from n in dc.Notification
                                    join u in dc.Users on n.CreatedBy equals u.ID
                                    join r in dc.Reports on n.ReportId equals r.ID
                                    orderby n.ID descending
                                    select new { n.ID, n.ReportId, r.ReportNumber, n.ActionType, n.ActionId, n.CreatedBy, UserName = $"{u.Firstname} {u.Surname}", n.CreatedDate }).Take(25);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult NotificationsByid(int Id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Notification() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from n in dc.Notification
                                    join u in dc.Users on n.CreatedBy equals u.ID
                                    join r in dc.Reports on n.ReportId equals r.ID
                                    where n.ID == Id
                                    orderby n.ID descending
                                    select new { n.ID, n.ReportId, r.ReportNumber, n.ActionType, n.ActionId, n.CreatedBy, UserName = $"{u.Firstname} {u.Surname}", n.CreatedDate }).Take(25);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult NotificationById(int Id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Notification() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var notification = (from n in dc.Notification
                                    join u in dc.Users on n.CreatedBy equals u.ID
                                    join r in dc.Reports on n.ReportId equals r.ID
                                    where n.ID == Id                                    
                                    select new { n.ID, n.ReportId, r.ReportNumber, n.ActionType, n.ActionId, n.CreatedBy, UserName = $"{u.Firstname} {u.Surname}", n.CreatedDate }).FirstOrDefault();
                        result.result = notification;
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult NotificationsByUser(string UserId)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Notification() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from n in dc.Notification
                                    join u in dc.Users on n.CreatedBy equals u.ID
                                    join r in dc.Reports on n.ReportId equals r.ID
                                    where n.CreatedBy == UserId
                                    orderby n.ID descending
                                    select new { n.ID, n.ReportId, r.ReportNumber, n.ActionType, n.ActionId, n.CreatedBy, UserName = $"{u.Firstname} {u.Surname}", n.CreatedDate }).Take(25);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
    }

    public class PushNotification
    {
        public int ReportId { get; set; }
        public string ReportNumber { get; set; }
        public string ActionType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public class PushNotificationSubscribe
    {
        public string UserId { get; set; }
        public string DeviceToken { get; set; }
    }
}