﻿using BellEquipment.Common;
using BellEquipment.Controllers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using static BellEquipment.Models.BellDbObject;

namespace BellEquipment.Models
{
    public class ReportModel : dbResult
    {
        static string connString = System.Configuration.ConfigurationManager.ConnectionStrings["bellDBConnectionString"].ConnectionString.ToString();
        //static NotificationController _pushController = new NotificationController();

        public static dbResult UserList()
        {
            dbResult result = new dbResult() { result = new List<Users>() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        //var list = from x in dc.Users where x.Name != null select x;

                        var list = (
                            from u in dc.Users
                            join ub in dc.UserBranch on u.ID equals ub.UserId
                            join b in dc.Branch on ub.BranchId equals b.ID
                            join db in dc.DealerBranch on b.ID equals db.BranchId
                            join d in dc.Dealers on db.DealersId equals d.ID
                            where ub.Status == true && b.Status == true && db.Status == true && d.Status == true
                            && (u.Firstname != "Admin" || u.Email != "vince@c.com") && u.PhoneNumber != null
                            select new
                            {
                                user = new { u.ID, u.Firstname, u.Surname, u.PhoneNumber, u.Email },
                                branch = new { b.ID, b.Name },
                                dealer = new { d.ID, d.Name, d.MobileNumber, d.Email }
                            });


                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }        

        public static dbResult User(string UserId)
        {
            dbResult result = new dbResult() { result = new Users() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        //var list = (from x in dc.Users where x.Name != null && x.UserId == UserId select x).Take(1);

                        var list = (
                            from u in dc.Users
                            join ub in dc.UserBranch on u.ID equals ub.UserId
                            join b in dc.Branch on ub.BranchId equals b.ID
                            join db in dc.DealerBranch on b.ID equals db.BranchId
                            join d in dc.Dealers on db.DealersId equals d.ID
                            where u.ID == UserId && ub.Status == true && b.Status == true && db.Status == true && d.Status == true
                            select new
                            {
                                user = new { u.ID, u.Firstname, u.Surname, u.PhoneNumber, u.Email, u.EmailNotificationsEnabled },
                                branch = new { b.ID, b.Name },
                                dealer = new { d.ID, d.Name, d.MobileNumber, d.Email }
                            }).Take(1);

                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult UserFind(string UserEmail)
        {
            dbResult result = new dbResult() { result = new Users() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.Users where x.Email.ToLower() == UserEmail.ToLower() select x).Take(1);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult DealerList()
        {
            dbResult result = new dbResult() { result = new List<Dealers>() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = from x in dc.Dealers where x.Name != null select x;
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult Dealer(int DealerId)
        {
            dbResult result = new dbResult() { result = new Dealers() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.Dealers where x.Name != null && x.ID == DealerId select x).Take(1);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ModelsCategory(int Id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Models() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.ModelsCategory where x.Status == true && x.Title != null && x.ID == Id select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ModelsCategoryList()
        {
            dbResult result = new dbResult() { result = new BellDbObject.Models() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.ModelsCategory where x.Status == true && x.Title != null select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult Models(int id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Models() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.Models where x.Status == true && x.Title != null && x.ID == id select new { x.ID, x.Title, x.ModelsCategoryId });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ModelsList()
        {
            dbResult result = new dbResult() { result = new BellDbObject.Models() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.Models where x.Status == true && x.Title != null select new { x.ID, x.Title, x.ModelsCategoryId });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult NonStandardAddonsByModelCategory(int id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.NonStandardAddons() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.NonStandardAddons where x.ModelsCategoryId == id select new { x.ID, x.Title, x.ModelsCategoryId });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult NonStandardAddons(int id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.NonStandardAddons() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.NonStandardAddons where x.ID == id select new { x.ID, x.Title, x.ModelsCategoryId });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult NonStandardAddons()
        {
            dbResult result = new dbResult() { result = new BellDbObject.NonStandardAddons() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.NonStandardAddons select new { x.ID, x.Title, x.ModelsCategoryId });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        private class ReportModelList
        {
            public int ModelsCategoryId { get; set; }
            public string ModelsCategoryTitle { get; set; }
            public List<object> Models { get; set; }
        }
        public static dbResult ModelsComplete(int id)
        {
            dbResult result = new dbResult() { result = new List<ReportModelList>() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from m in dc.Models
                                    join mc in dc.ModelsCategory on m.ModelsCategoryId equals mc.ID
                                    where m.Status == true && mc.Status == true && m.Title != null && mc.Title != null && mc.ID == id
                                    select new { ModelsCategoryId = mc.ID, ModelsCategoryTitle = mc.Title, ModelId = m.ID, ModelTitle = m.Title }
                                    );
                        // Create New Object to return
                        List<ReportModelList> rml = new List<ReportModelList>();
                        // Get unique list of category records
                        var categoryList = (from x in list
                                            select new { x.ModelsCategoryId, x.ModelsCategoryTitle }).Distinct().OrderBy(x => x.ModelsCategoryId);
                        // link each model to its respective category
                        foreach (var c in categoryList)
                        {
                            var modelList = (from x in list where x.ModelsCategoryId == c.ModelsCategoryId select new { x.ModelId, x.ModelTitle }).OrderBy(x => x.ModelId);
                            rml.Add(new ReportModelList() { ModelsCategoryId = c.ModelsCategoryId, ModelsCategoryTitle = c.ModelsCategoryTitle, Models = new List<object>(modelList) });
                        }
                        // return result
                        result.result = rml.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ModelsCompleteList()
        {
            dbResult result = new dbResult() { result = new List<ReportModelList>() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from m in dc.Models
                                    join mc in dc.ModelsCategory on m.ModelsCategoryId equals mc.ID
                                    where m.Status == true && mc.Status == true && m.Title != null && mc.Title != null
                                    select new { ModelsCategoryId = mc.ID, ModelsCategoryTitle = mc.Title, ModelId = m.ID, ModelTitle = m.Title }
                                    );
                        // Create New Object to return
                        List<ReportModelList> rml = new List<ReportModelList>();
                        // Get unique list of category records
                        var categoryList = (from x in list
                                            select new { x.ModelsCategoryId, x.ModelsCategoryTitle }).Distinct().OrderBy(x => x.ModelsCategoryId);
                        // link each model to its respective category
                        foreach (var c in categoryList)
                        {
                            var modelList = (from x in list where x.ModelsCategoryId == c.ModelsCategoryId select new { x.ModelId, x.ModelTitle }).OrderBy(x => x.ModelId);
                            rml.Add(new ReportModelList() { ModelsCategoryId = c.ModelsCategoryId, ModelsCategoryTitle = c.ModelsCategoryTitle, Models = new List<object>(modelList) });
                        }
                        // return result
                        result.result = rml.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult FaultCode()
        {
            dbResult result = new dbResult() { result = new BellDbObject.FaultCode() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.FaultCode where x.Status == true && x.Title != null select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult FaultCode(int Id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.FaultCode() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.FaultCode where x.Status == true && x.Title != null && x.ID == Id select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult FaultSubCode()
        {
            dbResult result = new dbResult() { result = new BellDbObject.FaultSubCode() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.FaultSubCode where x.Status == true && x.Title != null select new { x.ID, x.Title, x.FaultCodeId });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult FaultSubCode(int Id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.FaultSubCode() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.FaultSubCode where x.Status == true && x.Title != null && x.FaultCodeId == Id select new { x.ID, x.Title, x.FaultCodeId });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult FaultSubCodeById(int Id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.FaultSubCode() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.FaultSubCode where x.Status == true && x.Title != null && x.ID == Id select new { x.ID, x.Title, x.FaultCodeId });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ApplicationList()
        {
            dbResult result = new dbResult() { result = new BellDbObject.Application() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.Application where x.Status == true && x.Title != null select new { x.Id, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult Application(int Id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Application() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.Application where x.Status == true && x.Title != null && x.Id == Id select new { x.Id, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ReportStatus()
        {
            dbResult result = new dbResult() { result = new BellDbObject.ReportStatus() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.ReportStatus where x.Title != null && x.Status == true select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ReportStatus(int StatusId)
        {
            dbResult result = new dbResult() { result = new BellDbObject.ReportStatus() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.ReportStatus where x.Title != null && x.Status == true && x.ID == StatusId select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ReportStatus(string StatusTitle)
        {
            dbResult result = new dbResult() { result = new BellDbObject.ReportStatus() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.ReportStatus where x.Title != null && x.Status == true && x.Title == StatusTitle select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ReportType()
        {
            dbResult result = new dbResult() { result = new BellDbObject.ReportType() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.ReportType where x.Title != null && x.Status == true select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ReportType(int id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.ReportType() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.ReportType where x.Title != null && x.Status == true && x.ID == id select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ComplaintList()
        {
            dbResult result = new dbResult() { result = new BellDbObject.Complaint() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.Complaint where x.Status == true && x.Title != null select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult Complaint(int Id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Complaint() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.Complaint where x.Status == true && x.Title != null && x.ID == Id select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult DefectList()
        {
            dbResult result = new dbResult() { result = new BellDbObject.Defect() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.Defect where x.Status == true && x.Title != null select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult Defect(int Id)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Defect() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.Defect where x.Status == true && x.Title != null && x.ID == Id select new { x.ID, x.Title });
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        private class ReportFaultList
        {
            public int FaultCodeId { get; set; }
            public string FaultCodeTitle { get; set; }
            public List<object> FaultSubCode { get; set; }
        }
        public static dbResult FaultCompleteList()
        {
            dbResult result = new dbResult() { result = new List<ReportFaultList>() };
            try
            {
                // Query DB for complete JOIN list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from f in dc.FaultCode
                                    join s in dc.FaultSubCode on f.ID equals s.FaultCodeId
                                    where s.Status == true && f.Status == true && f.Title != null && s.Title != null
                                    select new { FaultId = f.ID, FaultTitle = f.Title, FaultSubId = s.ID, FaultSubTitle = s.Title }
                                    );
                        // Create New Object to return
                        List<ReportFaultList> resultList = new List<ReportFaultList>();
                        // Get unique list of category records
                        var mainList = (from x in list
                                        select new { x.FaultId, x.FaultTitle }).Distinct().OrderBy(x => x.FaultId);
                        // link each model to its respective category
                        foreach (var c in mainList)
                        {
                            var subList = (from x in list where x.FaultId == c.FaultId select new { x.FaultSubId, x.FaultSubTitle }).OrderBy(x => x.FaultSubId);
                            resultList.Add(new ReportFaultList() { FaultCodeId = c.FaultId, FaultCodeTitle = c.FaultTitle, FaultSubCode = new List<object>(subList) });
                            //result.result = (new ReportFaultList() { FaultCodeId = c.FaultId, FaultCodeTitle = c.FaultTitle, FaultSubCode = new List<object>(subList) });
                        }
                        // return result
                        result.result = resultList.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ReportList(bool order)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        if (!order)
                        {
                            var list = (from r in dc.Reports
                                        join m in dc.Models on r.ModelId equals m.ID into newModel
                                        from nModel in newModel.DefaultIfEmpty()
                                        join mc in dc.ModelsCategory on nModel.ModelsCategoryId equals mc.ID into newModelCat
                                        from nModCat in newModelCat.DefaultIfEmpty()
                                        where r.ReportNumber != null
                                        select new
                                        {
                                            r.ID,
                                            r.ReportNumber,
                                            r.ReportDate,
                                            r.ReportPersonId,
                                            r.DealersId,
                                            r.Customer,
                                            r.VinNo,
                                            r.ModelId,
                                            r.ReportTypeId,
                                            r.ReportedToId,
                                            r.FaultCodeId,
                                            r.FaultSubCodeId,
                                            r.NonStandardAddonsId,
                                            r.FaultCodeReported,
                                            r.FailedPartNumber,
                                            r.ReplacementPartNumber,
                                            r.ComplaintId,
                                            r.DefectId,
                                            r.ComplaintDescription,
                                            r.FailedComponentSN,
                                            r.RootCause,
                                            r.ActionRequired,
                                            r.SafetyHard,
                                            r.ApplicaitonId,
                                            r.Hours,
                                            r.Status,
                                            ModelsCategoryTitle = nModCat.Title
                                        }).Take(100);
                            result.result = list.ToList();
                        }
                        else
                        {
                            var list2 = (from r in dc.Reports
                                        join m in dc.Models on r.ModelId equals m.ID into newModel from nModel in newModel.DefaultIfEmpty()
                                        join mc in dc.ModelsCategory on nModel.ModelsCategoryId equals mc.ID into newModelCat from nModCat in newModelCat.DefaultIfEmpty()
                                        where r.ReportNumber != null
                                        orderby r.ID descending
                                         select new
                                         {
                                             r.ID,
                                             r.ReportNumber,
                                             r.ReportDate,
                                             r.ReportPersonId,
                                             r.DealersId,
                                             r.Customer,
                                             r.VinNo,
                                             r.ModelId,
                                             r.ReportTypeId,
                                             r.ReportedToId,
                                             r.FaultCodeId,
                                             r.FaultSubCodeId,
                                             r.NonStandardAddonsId,
                                             r.FaultCodeReported,
                                             r.FailedPartNumber,
                                             r.ReplacementPartNumber,
                                             r.ComplaintId,
                                             r.DefectId,
                                             r.ComplaintDescription,
                                             r.FailedComponentSN,
                                             r.RootCause,
                                             r.ActionRequired,
                                             r.SafetyHard,
                                             r.ApplicaitonId,
                                             r.Hours,
                                             r.Status,
                                             ModelsCategoryTitle = nModCat.Title
                                         }).Take(100);
                            result.result = list2.ToList();
                        }
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult Report(int ReportId)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.Reports where x.ReportNumber != null && x.ID == ReportId select x)
                            //.OrderBy(x => x.ReportDate)
                            .Take(1);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ReportFind(string ReportNumber)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.Reports where x.ReportNumber != null && x.ReportNumber.Contains(ReportNumber) select x);
                        /*
                         -- LIKE --
                         x.ReportNumber.Contains(ReportNumber)
                         x.ReportNumber.StartsWith(ReportNumber)
                         x.ReportNumber.EndsWith(ReportNumber)
                         -- IN --
                        x.ReportNumber.Contains(ReportNumber)
                         */
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ReportByUser(string UserId)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list =
                            from x in dc.Reports
                                //join u in dc.Users on x.ReportId equals u.UserId
                            where x.ReportNumber != null && x.ReportPersonId != null && (x.ReportedToId == UserId || x.ReportPersonId == UserId) //&& u.UserId == UserId
                            select x;
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ReportById(string UserId)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list =
                            from x in dc.Reports
                            //join u in dc.Users on x.ReportId equals u.ID
                            where x.ReportNumber != null && x.ReportPersonId != null 
                            && (x.ReportedToId == UserId || x.ReportPersonId == UserId)
                            //&& u.UserId == UserId
                            select x;
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        private static void SetCreatedBy(Report rpt, string UserId)
        {
            if (rpt != new Report() && !string.IsNullOrEmpty(UserId))
            {
                try
                {
                    using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                    {
                        using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                        {
                            ReportCreatedBy rptc = (from r in dc.ReportCreatedBy where r.ID == rpt.ID select r).FirstOrDefault();
                            rptc.CreatedBy = UserId;
                            dc.SubmitChanges();
                        }
                    }
                }
                catch (Exception e) {
                }
            }
        }

        public static dbResult ReportSave(Report ReportData, string UserId, string UserName)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                // Confirm that there is no existing report
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        result.success = true;

                        //bool existingReport = (dc.Reports.Where(r => r.ID == ReportData.ID).Count() > 0) ? true : false;
                        Report rpt = (from r in dc.Reports where r.ID == ReportData.ID select r).FirstOrDefault();
                        if (rpt == null)
                        {
                            // Create new draft report record
                            if (dc.Reports.OrderByDescending(r => r.ID).FirstOrDefault() != null)
                                ReportData.ReportNumber = string.Format("{0}{1}", (UserName.Length >= 3) ? UserName.Substring(0, 3) : "S", (dc.Reports.OrderByDescending(r => r.ID).FirstOrDefault().ID + 1).ToString());
                            else
                                ReportData.ReportNumber = string.Format("{0}{1}", (UserName.Length >= 3) ? UserName.Substring(0, 3) : "S", 1);

                            ReportData.Status = Convert.ToInt32(dc.ReportStatus.Where(s => s.Title == "Draft").Select(s => s.ID).Single());

                            dc.Reports.InsertOnSubmit(ReportData);
                            try
                            {
                                dc.SubmitChanges();
                                result.result = ReportData;
                                SetCreatedBy(ReportData, UserId);
                            }
                            catch (Exception ex)
                            {
                                result.success = false;
                                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
                            }
                        }
                        else
                        {
                            // Update existing record without changing anything.
                            #region Update Report Record
                            rpt.ReportNumber = ReportData.ReportNumber;
                            rpt.ReportDate = ReportData.ReportDate;
                            rpt.ReportPersonId = ReportData.ReportPersonId;
                            rpt.DealersId = ReportData.DealersId;
                            rpt.Customer = ReportData.Customer;
                            rpt.VinNo = ReportData.VinNo;
                            rpt.ModelId = ReportData.ModelId;
                            rpt.ReportTypeId = ReportData.ReportTypeId;
                            rpt.ReportedToId = ReportData.ReportedToId;
                            rpt.FaultCodeId = ReportData.FaultCodeId;
                            rpt.FaultSubCodeId = ReportData.FaultSubCodeId;
                            rpt.NonStandardAddonsId = ReportData.NonStandardAddonsId;
                            rpt.FaultCodeReported = ReportData.FaultCodeReported;
                            rpt.FailedPartNumber = ReportData.FailedPartNumber;
                            rpt.ReplacementPartNumber = ReportData.ReplacementPartNumber;
                            rpt.ComplaintId = ReportData.ComplaintId;
                            rpt.DefectId = ReportData.DefectId;
                            rpt.ComplaintDescription = ReportData.ComplaintDescription;
                            rpt.FailedComponentSN = ReportData.FailedComponentSN;
                            rpt.RootCause = ReportData.RootCause;
                            rpt.ActionRequired = ReportData.ActionRequired;
                            rpt.SafetyHard = ReportData.SafetyHard;
                            rpt.ApplicaitonId = ReportData.ApplicaitonId;
                            rpt.Hours = ReportData.Hours;
                            #endregion
                            try
                            {
                                // Try and save changes to DB
                                dc.SubmitChanges();
                                result.result = rpt;
                            }
                            catch (Exception ex)
                            {
                                result.success = false;
                                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ReportSubmit(Report ReportData, string activeUserId)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                // Confirm that there is no existing report
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        result.success = true;

                        //bool existingReport = (dc.Reports.Where(r => r.ID == ReportData.ID).Count() > 0) ? true : false;
                        //int reportStatus = (dc.Reports.Where(r => r.ID == ReportData.ID).Select(x => x.Status).FirstOrDefault());
                        Report rpt = (from r in dc.Reports where r.ID == ReportData.ID select r).FirstOrDefault();
                        int existingStatus = rpt.Status;
                        if (rpt != null)
                        {
                            int StatusId = Convert.ToInt32(dc.ReportStatus.Where(s => s.Title == "Open").Select(s => s.ID).Single());

                            // Get Report Record
                            //Report rpt = (from r in dc.Reports where r.ID == ReportData.ID select r).FirstOrDefault();
                            if (StatusId > 0 && rpt != null)
                            {
                                #region Update Report Record
                                rpt.ReportNumber = ReportData.ReportNumber;
                                rpt.ReportDate = ReportData.ReportDate;
                                rpt.ReportPersonId = ReportData.ReportPersonId;
                                rpt.DealersId = ReportData.DealersId;
                                rpt.Customer = ReportData.Customer;
                                rpt.VinNo = ReportData.VinNo;
                                rpt.ModelId = ReportData.ModelId;
                                rpt.ReportTypeId = ReportData.ReportTypeId;
                                rpt.ReportedToId = ReportData.ReportedToId;
                                rpt.FaultCodeId = ReportData.FaultCodeId;
                                rpt.FaultSubCodeId = ReportData.FaultSubCodeId;
                                rpt.NonStandardAddonsId = ReportData.NonStandardAddonsId;
                                rpt.FaultCodeReported = ReportData.FaultCodeReported;
                                rpt.FailedPartNumber = ReportData.FailedPartNumber;
                                rpt.ReplacementPartNumber = ReportData.ReplacementPartNumber;
                                rpt.ComplaintId = ReportData.ComplaintId;
                                rpt.DefectId = ReportData.DefectId;
                                rpt.ComplaintDescription = ReportData.ComplaintDescription;
                                rpt.FailedComponentSN = ReportData.FailedComponentSN;
                                rpt.RootCause = ReportData.RootCause;
                                rpt.ActionRequired = ReportData.ActionRequired;
                                rpt.SafetyHard = ReportData.SafetyHard;
                                rpt.ApplicaitonId = ReportData.ApplicaitonId;
                                rpt.Hours = ReportData.Hours;
                                rpt.Status = StatusId; 
                                #endregion
                                try
                                {
                                    // Try and save changes to DB
                                    dc.SubmitChanges();
                                    int notificationId = 0;
                                    if (existingStatus != StatusId)
                                    {
                                        notificationId = NotificationModel.CreateNotificationAsync(rpt.ID.ToString(), NotificationModel.NotificationTypes.ReportCreated.GetDescription(), null, activeUserId);
                                    }
                                    else
                                    {
                                        notificationId = NotificationModel.CreateNotificationAsync(rpt.ID.ToString(), NotificationModel.NotificationTypes.ReportUpdated.GetDescription(), null, activeUserId);
                                    }

                                    if (notificationId > 0)
                                    {
                                        //PUSH NOTIFICATION
                                        //_pushController.NotificationCallBackMessage(notificationId);
                                        FirebasePushNotification.SendPushNotification(notificationId, activeUserId);
                                    }
                                    
                                    result.result = rpt;                                    
                                }
                                catch (Exception ex)
                                {
                                    result.success = false;
                                    result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ReportClosed(Report ReportData, string activeUserId)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                // Confirm that there is no existing report
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        result.success = true;

                        bool existingReport = (dc.Reports.Where(r => r.ID == ReportData.ID).Count() > 0) ? true : false;
                        if (existingReport)
                        {
                            int StatusId = Convert.ToInt32(dc.ReportStatus.Where(s => s.Title == "Closed").Select(s => s.ID).Single());

                            //var rpt = dc.Reports.First(x => x.ID == ReportData.ID);
                            //rpt.Status = StatusId;

                            //Report rpt = dc.Reports.Single(x => x.ID == ReportData.ID);//.Where(x => x.ID == ReportData.ID).SingleOrDefault();

                            // Get Report Record
                            Report rpt = (from r in dc.Reports where r.ID == ReportData.ID select r).FirstOrDefault();
                            if (StatusId > 0 && rpt != null)
                            {
                                #region Update Report
                                rpt.ReportNumber = ReportData.ReportNumber;
                                rpt.ReportDate = ReportData.ReportDate;
                                rpt.ReportPersonId = ReportData.ReportPersonId;
                                rpt.DealersId = ReportData.DealersId;
                                rpt.Customer = ReportData.Customer;
                                rpt.VinNo = ReportData.VinNo;
                                rpt.ModelId = ReportData.ModelId;
                                rpt.ReportTypeId = ReportData.ReportTypeId;
                                rpt.ReportedToId = ReportData.ReportedToId;
                                rpt.FaultCodeId = ReportData.FaultCodeId;
                                rpt.FaultSubCodeId = ReportData.FaultSubCodeId;
                                rpt.NonStandardAddonsId = ReportData.NonStandardAddonsId;
                                rpt.FaultCodeReported = ReportData.FaultCodeReported;
                                rpt.FailedPartNumber = ReportData.FailedPartNumber;
                                rpt.ReplacementPartNumber = ReportData.ReplacementPartNumber;
                                rpt.ComplaintId = ReportData.ComplaintId;
                                rpt.DefectId = ReportData.DefectId;
                                rpt.ComplaintDescription = ReportData.ComplaintDescription;
                                rpt.FailedComponentSN = ReportData.FailedComponentSN;
                                rpt.RootCause = ReportData.RootCause;
                                rpt.ActionRequired = ReportData.ActionRequired;
                                rpt.SafetyHard = ReportData.SafetyHard;
                                rpt.ApplicaitonId = ReportData.ApplicaitonId;
                                rpt.Hours = ReportData.Hours;
                                rpt.Status = StatusId; 
                                #endregion
                                try
                                {
                                    // Try and save changes to DB
                                    dc.SubmitChanges();
                                    result.result = rpt;

                                    int notificationId = 0;
                                    notificationId = NotificationModel.CreateNotificationAsync(rpt.ID.ToString(), NotificationModel.NotificationTypes.ReportClosed.GetDescription(), null, activeUserId);

                                    if (notificationId > 0)
                                    {
                                        //PUSH NOTIFICATION
                                        //_pushController.NotificationCallBackMessage(notificationId);
                                        FirebasePushNotification.SendPushNotification(notificationId, activeUserId);
                                    }

                                    //NotificationModel.NotificationCreateAsync(rpt.ID.ToString(), NotificationModel.NotificationTypes.ReportClosed.GetDescription(), null, activeUserId);
                                }
                                //catch (System.Data.Linq.ChangeConflictException cce)
                                //{
                                //    //errorList.Add(cce.Message.ToString());
                                //}
                                catch (Exception ex)
                                {
                                    result.success = false;
                                    result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static ReportGeneratedObject ReportGenerateEmail(System.Web.Mvc.ControllerContext cc, string ReportID)
        {
            ReportGeneratedObject result = new ReportGeneratedObject();
            try
            {
                // Confirm that there is no existing report
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        Report rpt = (from r in dc.Reports where r.ID.ToString() == ReportID select r).FirstOrDefault();
                        if (rpt != null)
                        { 
                            result.ReportObj = rpt;
                            result.FaultCodeObj = (from x in dc.FaultCode where x.ID == rpt.FaultCodeId select x).FirstOrDefault();
                            result.FaultSubCodeObj = (from x in dc.FaultSubCode where x.ID == rpt.FaultSubCodeId select x).FirstOrDefault();
                            result.UsersList = (from x in dc.Users where x.ID == rpt.ReportedToId || x.ID == rpt.ReportPersonId select x).ToList();
                            result.StatusObj = (from x in dc.ReportStatus where x.ID == rpt.Status select x).FirstOrDefault();
                            result.PurposeObj = (from x in dc.ReportType where x.ID == rpt.ReportTypeId select x).FirstOrDefault();
                            result.ModelObj = (from x in dc.Models where x.ID == rpt.ModelId select x).FirstOrDefault();
                            result.ModelsCategoryObj = result.ModelObj == null ? null : (from x in dc.ModelsCategory where x.ID == result.ModelObj.ModelsCategoryId select x).FirstOrDefault();
                            result.ApplicationObj = (from x in dc.Application where x.Id == rpt.ApplicaitonId select x).FirstOrDefault();
                            result.ComplaintObj = (from x in dc.Complaint where x.ID == rpt.ComplaintId select x).FirstOrDefault();
                            result.NonStandardAddonObj = (from x in dc.NonStandardAddons where x.ID == rpt.NonStandardAddonsId select x).FirstOrDefault();
                            result.DefectObj = (from x in dc.Defect where x.ID == rpt.DefectId select x).FirstOrDefault();
                            var list = from d in dc.DocumentTable join rd in dc.ReportDocuments on d.ID equals rd.DocumentId where rd.ReportId == rpt.ID && !rd.IsRemoved select d ;
                            result.Images = list.ToList();
                            result.success = true;

                            var pdfResult = new Rotativa.ViewAsPdf("ReportPDF", result) { FileName = string.Format("Report {0}.pdf", rpt.ReportNumber) };
                            byte[] applicationPDFData = pdfResult.BuildPdf(cc);
                            System.IO.MemoryStream file = new System.IO.MemoryStream(applicationPDFData);
                            file.Seek(0, System.IO.SeekOrigin.Begin);
                            System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(file, string.Format("Report {0}.pdf", rpt.ReportNumber), "application/pdf");

                            BellEquipment.Models.Email.SendEmail(
                                new List<string>(result.UsersList.Where(x => !string.IsNullOrEmpty(x.Email) && x.EmailNotificationsEnabled).Select(x => x.Email).ToList()),
                                $"FIR REPORT {rpt.ReportNumber} HAS BEEN SUBMITTED",
                                $" Hi There, </br><p> Please note find attached copy of FIR Report {rpt.ReportNumber}</p>",
                                attachment
                                );
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = e.Message;
            }
            return result;
        }


        public class ReportGeneratedObject
        {
            public Report ReportObj { get; set; }
            public FaultCode FaultCodeObj { get; set; }
            public FaultSubCode FaultSubCodeObj { get; set; }
            public List<Users> UsersList { get; set; }
            public ReportStatus StatusObj { get; set; }
            public string error { get; set; }
            public bool success { get; set; }
            public ReportType PurposeObj { get; set; }
            public Application ApplicationObj { get; set; }
            public Complaint ComplaintObj { get; set; }
            public NonStandardAddons NonStandardAddonObj { get; set; }
            public Defect DefectObj { get; set; }
            public BellDbObject.Models ModelObj { get; set; }
            public ModelsCategory ModelsCategoryObj { get; set; }
            public List<DocumentTable> Images { get; set; }
        }

        public static ReportGeneratedObject ReportGenerateView(string ReportID)
        {
            ReportGeneratedObject result = new ReportGeneratedObject();
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        Report rpt = (from r in dc.Reports where r.ID.ToString() == ReportID select r).FirstOrDefault();
                        if (rpt != null)
                            result.ReportObj = rpt;
                        result.FaultCodeObj = (from x in dc.FaultCode where x.ID == rpt.FaultCodeId select x).FirstOrDefault();
                        result.FaultSubCodeObj = (from x in dc.FaultSubCode where x.ID == rpt.FaultSubCodeId select x).FirstOrDefault();
                        result.UsersList = (from x in dc.Users where x.ID == rpt.ReportedToId || x.ID == rpt.ReportPersonId select x).ToList();
                        result.StatusObj = (from x in dc.ReportStatus where x.ID == rpt.Status select x).FirstOrDefault();
                    }
                    result.success = true;
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }


        public static dbResult ReportDuplicate(int ReportId)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                // Confirm that there is no existing report
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        result.success = true;

                        bool existingReport = (dc.Reports.Where(r => r.ID == ReportId).Count() > 0) ? true : false;
                        if (existingReport)
                        {
                            Report rpt = dc.Reports.First(r => r.ID == ReportId);
                            Report duplicateReport = new Report
                            {
                                #region Update Data
                                ReportNumber = rpt.ReportNumber,
                                ReportDate = rpt.ReportDate,
                                ReportPersonId = rpt.ReportPersonId,
                                DealersId = rpt.DealersId,
                                Customer = rpt.Customer,
                                VinNo = rpt.VinNo,
                                ModelId = rpt.ModelId,
                                ReportTypeId = rpt.ReportTypeId,
                                ReportedToId = rpt.ReportedToId,
                                FaultCodeId = rpt.FaultCodeId,
                                FaultSubCodeId = rpt.FaultSubCodeId,
                                NonStandardAddonsId = rpt.NonStandardAddonsId,
                                FaultCodeReported = rpt.FaultCodeReported,
                                FailedPartNumber = rpt.FailedPartNumber,
                                ReplacementPartNumber = rpt.ReplacementPartNumber,
                                ComplaintId = rpt.ComplaintId,
                                DefectId = rpt.DefectId,
                                ComplaintDescription = rpt.ComplaintDescription,
                                FailedComponentSN = rpt.FailedComponentSN,
                                RootCause = rpt.RootCause,
                                ActionRequired = rpt.ActionRequired,
                                SafetyHard = rpt.SafetyHard,
                                ApplicaitonId = rpt.ApplicaitonId,
                                Hours = rpt.Hours,
                                Status = Convert.ToInt32(dc.ReportStatus.Where(s => s.Title == "Draft").Select(s => s.ID).Single())
                                #endregion
                            };

                            dc.Reports.InsertOnSubmit(duplicateReport);
                            try
                            {
                                dc.SubmitChanges();
                                result.result = duplicateReport;
                            }
                            catch (Exception ex)
                            {
                                result.success = false;
                                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
                            }
                        }
                        else
                        {
                            result.success = false;
                            result.error = ErrorHandler.ValidationError.DatabaseRecordError.GetDescription();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ReportDuplicateFromModel(int ReportId,string VinNo, decimal Hours)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                // Confirm that there is no existing report
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        result.success = true;

                        bool existingReport = (dc.Reports.Where(r => r.ID == ReportId).Count() > 0) ? true : false;
                        if (existingReport)
                        {
                            Report rpt = dc.Reports.First(r => r.ID == ReportId);
                            Report duplicateReport = new Report
                            {
                                ReportNumber = rpt.ReportNumber,
                                ReportDate = rpt.ReportDate,
                                ReportPersonId = rpt.ReportPersonId,
                                DealersId = rpt.DealersId,
                                Customer = rpt.Customer,
                                VinNo = VinNo,
                                ModelId = rpt.ModelId,
                                ReportTypeId = rpt.ReportTypeId,
                                ReportedToId = rpt.ReportedToId,
                                FaultCodeId = rpt.FaultCodeId,
                                FaultSubCodeId = rpt.FaultSubCodeId,
                                NonStandardAddonsId = rpt.NonStandardAddonsId,
                                FaultCodeReported = rpt.FaultCodeReported,
                                FailedPartNumber = rpt.FailedPartNumber,
                                ReplacementPartNumber = rpt.ReplacementPartNumber,
                                ComplaintId = rpt.ComplaintId,
                                DefectId = rpt.DefectId,
                                ComplaintDescription = rpt.ComplaintDescription,
                                RootCause = rpt.RootCause,
                                ActionRequired = rpt.ActionRequired,
                                SafetyHard = rpt.SafetyHard,
                                ApplicaitonId = rpt.ApplicaitonId,
                                Hours = Hours,
                                Status = Convert.ToInt32(dc.ReportStatus.Where(s => s.Title == "Draft").Select(s => s.ID).Single())
                            };

                            dc.Reports.InsertOnSubmit(duplicateReport);
                            try
                            {
                                dc.SubmitChanges();
                                result.result = duplicateReport;
                            }
                            catch (Exception ex)
                            {
                                result.success = false;
                                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
                            }
                        }
                        else
                        {
                            result.success = false;
                            result.error = ErrorHandler.ValidationError.DatabaseRecordError.GetDescription();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult ReportByStatus(int id)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from x in dc.Reports where x.Status == id select x);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult ReportByStatus(string status)
        {
            dbResult result = new dbResult() { result = new Report() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list = (from r in dc.Reports
                                    join s in dc.ReportStatus on r.Status equals s.ID
                                    where s.Title == status
                                    select r);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        private class StatsUser
        {
            public int ReportOpen { get; set; }
            public int ReportDraft { get; set; }
            public int ReportClosed { get; set; }
            public int ReportTotal { get; set; }
        }
        public static dbResult StatsByUserId(string UserId, DateTime Month)
        {
            dbResult result = new dbResult() { result = new StatsUser() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        //var list = (from x in dc.Reports  where x.ReportNumber != null select x);
                        // List of the status
                        var statusList = dc.ReportStatus;
                        // List of Reports Linked to user
                        var listComplete = dc.Reports
                                .Where(r => r.ReportNumber != null
                                /* MORE PARAMS  */
                                && r.ReportPersonId == UserId
                                && r.ReportDate.Month == Month.Month
                                );
                        // Count per Title
                        var listResult = from lc in statusList
                                         join gc in (from l in listComplete group l by new { l.Status } into g select new { g.Key.Status, Number = g.Count() }) on lc.ID equals gc.Status
                                         select new { lc.ID, lc.Title, gc.Number };
                        // return results
                        result.result = new StatsUser()
                        {
                            ReportOpen = listResult.Any(x => x.Title == "Open") ? (listResult.Where(x => x.Title == "Open").Select(x => x.Number).First()) : 0,
                            ReportDraft = listResult.Any(x => x.Title == "Draft") ? (listResult.Where(x => x.Title == "Draft").Select(x => x.Number).First()) : 0,
                            ReportClosed = listResult.Any(x => x.Title == "Closed") ? (listResult.Where(x => x.Title == "Closed").Select(x => x.Number).First()) : 0,
                            ReportTotal = listResult.Any() ? listResult.Sum(x => x.Number) : 0,
                        };
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult StatsByUserPeriod(string UserId, DateTime StartMonth, DateTime EndMonth)
        {
            dbResult result = new dbResult() { result = new StatsUser() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        //var list = (from x in dc.Reports  where x.ReportNumber != null select x);
                        // List of the status
                        var statusList = dc.ReportStatus;
                        // List of Reports Linked to user
                        var listComplete = dc.Reports
                                .Where(r => r.ReportNumber != null
                                /* MORE PARAMS  */
                                && r.ReportPersonId == UserId
                                && r.ReportDate >= StartMonth
                                && r.ReportDate <= EndMonth
                                );
                        // Count per Title
                        var listResult = from lc in statusList
                                         join gc in (from l in listComplete group l by new { l.Status } into g select new { g.Key.Status, Number = g.Count() }) on lc.ID equals gc.Status
                                         select new { lc.ID, lc.Title, gc.Number };
                        // return results
                        result.result = new StatsUser()
                        {
                            ReportOpen = listResult.Any(x => x.Title == "Open") ? (listResult.Where(x => x.Title == "Open").Select(x => x.Number).First()) : 0,
                            ReportDraft = listResult.Any(x => x.Title == "Draft") ? (listResult.Where(x => x.Title == "Draft").Select(x => x.Number).First()) : 0,
                            ReportClosed = listResult.Any(x => x.Title == "Closed") ? (listResult.Where(x => x.Title == "Closed").Select(x => x.Number).First()) : 0,
                            ReportTotal = listResult.Any() ? listResult.Sum(x => x.Number) : 0,
                        };
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult MessagesByReportId(int ReportId)
        {
            dbResult result = new dbResult() { result = new object() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list =
                            from r in dc.Reports
                            join rm in dc.ReportMessages on r.ID equals rm.ReportId
                            join m in dc.Messages on rm.MessageId equals m.ID
                            where r.ID == ReportId
                            orderby m.CreatedDate
                            select new { rm.UserId, rm.MessageId, m.Post, m.IsDocument, m.CreatedDate };
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult MessageSavePost(int ReportId, string UserId, string NewMessage)
        {
            dbResult result = new dbResult() { result = new object() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        // Add new Post
                        Messages msg = new Messages { Post = NewMessage };
                        dc.Messages.InsertOnSubmit(msg);
                        try
                        {
                            dc.SubmitChanges();
                            // Insert into Linked Table
                            ReportMessages rptmsg = new ReportMessages { ReportId = ReportId, UserId = UserId, MessageId = Convert.ToInt32(msg.ID) };
                            dc.ReportMessages.InsertOnSubmit(rptmsg);
                            dc.SubmitChanges();

                            int notificationId = NotificationModel.CreateNotificationAsync(ReportId.ToString(), NotificationModel.NotificationTypes.ReportMessage.GetDescription(), msg.ID.ToString(), UserId);

                            if (notificationId > 0)
                            {
                                //_pushController.NotificationCallBackMessage(notificationId);
                                FirebasePushNotification.SendPushNotification(notificationId, UserId);
                            }                            

                            result.result = new { rptmsg.UserId, rptmsg.MessageId, msg.Post, msg.CreatedDate };
                            result.success = true;
                            
                            
                            //NotificationModel.NotificationCreateAsync(ReportId.ToString(), NotificationModel.NotificationTypes.ReportMessage.GetDescription(), msg.ID.ToString(), UserId);
                        }
                        catch (Exception ex)
                        {
                            result.result = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        public static dbResult MessageDocumentDetail(int MessageId)
        {
            dbResult result = new dbResult() { result = new object() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list =
                            from d in dc.DocumentTable
                            join md in dc.MessageDocuments on d.ID equals md.DocumentId
                            where md.MessageId == MessageId && !md.IsRemoved
                            select new { d.ID, d.FileName, d.Extension, d.CreatedBy, d.CreatedDate };
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult MessageSaveDocument(int ReportId, string UserId, List<HttpPostedFile> fileCollection)
        {
            dbResult result = new dbResult { result = new object() };
            try
            {
                List<object> ErrorList = new List<object>();
                List<object> FileResult = new List<object>();
                foreach (HttpPostedFile file in fileCollection)
                {
                    DocumentTable d = new DocumentTable { FileName = file.FileName, Extension = file.ContentType, CreatedBy = UserId, CreatedDate = DateTime.Now };
                    using (System.IO.Stream fs = file.InputStream)
                    {
                        using (System.IO.BinaryReader br = new System.IO.BinaryReader(fs))
                        {
                            Byte[] fileBytes = br.ReadBytes((Int32)fs.Length);
                            d.DocumentContent = (new System.Data.Linq.Binary(fileBytes)).ToString();
                        }
                    }
                    // Save to DB
                    using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                    {
                        using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                        { 
                            //DocumentTable d = new DocumentTable { FileName = FileName, Extension = FileContentType, DocumentContent = FileData };
                            dc.DocumentTable.InsertOnSubmit(d);
                            try
                            {
                                dc.SubmitChanges();
                                Messages msg = new Messages { Post = string.Empty, IsDocument = true };
                                dc.Messages.InsertOnSubmit(msg);
                                dc.SubmitChanges();

                                // Add link to Message table
                                MessageDocuments md = new MessageDocuments() { MessageId = Convert.ToInt32(msg.ID), DocumentId = d.ID, IsRemoved = false };
                                dc.MessageDocuments.InsertOnSubmit(md);
                                dc.SubmitChanges();

                                // Insert into Linked Table
                                ReportMessages rptmsg = new ReportMessages { ReportId = ReportId, UserId = UserId, MessageId = Convert.ToInt32(msg.ID) };
                                dc.ReportMessages.InsertOnSubmit(rptmsg);
                                dc.SubmitChanges();

                                FileResult.Add(new { UserId, MessageId = msg.ID, msg.Post, msg.CreatedDate });
                            }
                            catch (Exception ex)
                            {
                                ErrorList.Add(new { d.FileName, ex.Message });
                            }
                            result.result = FileResult;
                            result.success = true;
                            result.error = ErrorList;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
            }
            return result;
        }
        public static dbResult MessageSaveDocumentBase64(int ReportId, string UserId, List<DocumentTable> docs)
        {
            dbResult result = new dbResult { result = new object() };
            try
            {
                List<object> ErrorList = new List<object>();
                List<object> FileResult = new List<object>();
                foreach (DocumentTable file in docs)
                {
                    if (file != new DocumentTable()
                        && !string.IsNullOrEmpty(file.FileName) && !string.IsNullOrEmpty(file.Extension)
                        && !string.IsNullOrEmpty(file.DocumentContent.ToString())
                        )
                    {
                        DocumentTable d = new DocumentTable { FileName = file.FileName, Extension = file.Extension, DocumentContent = file.DocumentContent, CreatedBy = UserId, CreatedDate = DateTime.Now };
                        // Save to DB
                        using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                        {
                            using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                            { 
                                dc.DocumentTable.InsertOnSubmit(d);
                                try
                                {
                                    dc.SubmitChanges();
                                    Messages msg = new Messages { Post = string.Empty, IsDocument = true };
                                    dc.Messages.InsertOnSubmit(msg);
                                    dc.SubmitChanges();

                                    // Add link to Message table
                                    MessageDocuments md = new MessageDocuments() { MessageId = Convert.ToInt32(msg.ID), DocumentId = d.ID, IsRemoved = false };
                                    dc.MessageDocuments.InsertOnSubmit(md);
                                    dc.SubmitChanges();

                                    // Insert into Linked Table
                                    ReportMessages rptmsg = new ReportMessages { ReportId = ReportId, UserId = UserId, MessageId = Convert.ToInt32(msg.ID) };
                                    dc.ReportMessages.InsertOnSubmit(rptmsg);
                                    dc.SubmitChanges();

                                    FileResult.Add(new { UserId, MessageId = msg.ID, msg.Post, msg.CreatedDate });

                                    int notificationId = NotificationModel.CreateNotificationAsync(ReportId.ToString(), NotificationModel.NotificationTypes.ReportMessage.GetDescription(), msg.ID.ToString(), UserId);

                                    if (notificationId > 0)
                                    {
                                        //_pushController.NotificationCallBackMessage(notificationId);
                                        FirebasePushNotification.SendPushNotification(notificationId, UserId);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorList.Add(new { d.FileName, ex.Message });
                                }
                            }
                        }
                    }
                    else
                    {
                        ErrorList.Add(new { file });
                    }
                    result.result = FileResult;
                    result.success = true;
                    result.error = ErrorList;
                }
            }
            catch (Exception ex)
            {
                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
            }
            return result;
            
        }

        public static dbResult DocumentsById(int Id)
        {
            dbResult result = new dbResult() { result = new object() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list =
                            from d in dc.DocumentTable
                            where d.ID == Id
                            select new { d.ID, d.FileName, d.Extension, d.DocumentContent, d.CreatedBy, d.CreatedDate };
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult DocumentsByReport(int Id)
        {
            dbResult result = new dbResult() { result = new object() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        var list =
                            from d in dc.DocumentTable
                            join rd in dc.ReportDocuments on d.ID equals rd.DocumentId
                            where rd.ReportId == Id && !rd.IsRemoved
                            select new { d.ID, d.FileName, d.Extension, d.CreatedBy, d.CreatedDate };
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult DocumentsSave(int ReportId, string UserId, List<HttpPostedFile> fileCollection) //HttpFileCollection fileCollection)
        {
            dbResult result = new dbResult { result = new object() };
            try
            {
                List<object> ErrorList = new List<object>();
                List<object> FileResult = new List<object>();
                foreach (HttpPostedFile file in fileCollection)
                {
                    DocumentTable d = new DocumentTable { FileName = file.FileName, Extension = file.ContentType, CreatedBy = UserId, CreatedDate = DateTime.Now };
                    using (System.IO.Stream fs = file.InputStream)
                    {
                        using (System.IO.BinaryReader br = new System.IO.BinaryReader(fs))
                        {
                            Byte[] fileBytes = br.ReadBytes((Int32)fs.Length);
                            d.DocumentContent = Convert.ToBase64String(fileBytes);
                        }
                    }
                    // Save to DB
                    using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                    {
                        using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                        { 
                            //DocumentTable d = new DocumentTable { FileName = FileName, Extension = FileContentType, DocumentContent = FileData };
                            dc.DocumentTable.InsertOnSubmit(d);
                            try
                            {
                                dc.SubmitChanges();

                                // Add link to report table
                                ReportDocuments rd = new ReportDocuments() { ReportId = ReportId, DocumentId = d.ID };
                                dc.ReportDocuments.InsertOnSubmit(rd);
                                dc.SubmitChanges();

                                FileResult.Add(new { d.ID, d.FileName, d.Extension, rd.ReportId});
                            }
                            catch (Exception ex)
                            {
                                ErrorList.Add(new { d.FileName, ex.Message });
                            }
                            result.result = FileResult;
                            result.success = true;
                            result.error = ErrorList;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
            }
            return result;
        }

        public class DocListBase64String
        {
            public int ReportId { get; set; }
            public List<DocumentTable> Docs { get; set; }
        }
        public static dbResult DocumentsSaveBase64(int ReportId, string UserId, List<DocumentTable> docs)
        {
            dbResult result = new dbResult { result = new object() };
            try
            {
                List<object> ErrorList = new List<object>();
                List<object> FileResult = new List<object>();
                foreach (DocumentTable file in docs)
                {
                    if (file != new DocumentTable()
                        && !string.IsNullOrEmpty(file.FileName) && !string.IsNullOrEmpty(file.Extension)
                        && !string.IsNullOrEmpty(file.DocumentContent.ToString())
                        )
                    {
                        DocumentTable d = new DocumentTable { FileName = file.FileName, Extension = file.Extension, DocumentContent = file.DocumentContent, CreatedBy = UserId, CreatedDate = DateTime.Now };
                        // Save to DB
                        using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                        {
                            using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                            { 
                                dc.DocumentTable.InsertOnSubmit(d);
                                try
                                {
                                    dc.SubmitChanges();

                                    // Add link to report table
                                    ReportDocuments rd = new ReportDocuments() { ReportId = ReportId, DocumentId = d.ID };
                                    dc.ReportDocuments.InsertOnSubmit(rd);
                                    dc.SubmitChanges();

                                    FileResult.Add(new { d.ID, d.FileName, d.Extension, rd.ReportId });
                                }
                                catch (Exception ex)
                                {
                                    ErrorList.Add(new { d.FileName, ex.Message });
                                }
                            }
                        }
                    }
                    else
                    {
                        ErrorList.Add(new { file });
                    }
                    result.result = FileResult;
                    result.success = true;
                    result.error = ErrorList;
                }
            }
            catch (Exception ex)
            {
                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
            }
            return result;
        }

        public static dbResult DocumentRemove(int ReportId, int DocumentId)
        {
            dbResult result = new dbResult() { result = new object() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    { 
                        bool existingDocument = (dc.ReportDocuments.Where(r => r.ReportId == ReportId && r.DocumentId == DocumentId).Count() > 0) ? true : false;
                        if (existingDocument)
                        {
                            ReportDocuments reportDoc = (from rd in dc.ReportDocuments where rd.ReportId == ReportId && rd.DocumentId == DocumentId select rd).FirstOrDefault();
                            if (reportDoc.IsRemoved)
                            {
                                result.result = new { reportDoc.ReportId, reportDoc.DocumentId, reportDoc.IsRemoved };
                                result.success = true;
                                return result;
                            }
                            else
                                try
                                {
                                    reportDoc.IsRemoved = true;
                                    // Try and save changes to DB
                                    dc.SubmitChanges();
                                    //result.result = reportDoc;
                                    result.result = new { reportDoc.ReportId, reportDoc.DocumentId, reportDoc.IsRemoved };
                                    result.success = true;
                                }
                                catch (Exception ex)
                                {
                                    result.success = false;
                                    result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), ex.Message);
                                }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

        #region Interal Calls (Usermanagement Portal)
        public static dbResult InternalUserList()
        {
            dbResult result = new dbResult() { result = new List<Users>() };

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    using (BellEquipDataContext ctx = new BellEquipDataContext(conn))
                    {
                        var list = ctx.Users.Select(s => new { ID = s.ID, Email = s.Email, Firstname = s.Firstname, Surname = s.Surname, PhoneNumber = s.PhoneNumber });

                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result.error = ex.Message;
            }
            return result;
        }

        public static dbResult InternalUser(string Id)
        {
            dbResult result = new dbResult() { result = new object() };

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    using (BellEquipDataContext ctx = new BellEquipDataContext(conn))
                    {
                        var user = (from u in ctx.Users
                                    join ub in ctx.UserBranch on u.ID equals ub.UserId into ubg
                                    from ub in ubg.DefaultIfEmpty()
                                    join b in ctx.Branch on ub.BranchId equals b.ID into bg
                                    from b in bg.DefaultIfEmpty()
                                    join db in ctx.DealerBranch on  b.ID equals db.BranchId into dbg
                                    from db in dbg.DefaultIfEmpty()
                                    join d in ctx.Dealers on db.DealersId equals d.ID into dg                                    
                                    from d in dg.DefaultIfEmpty()
                                    where u.ID == Id
                                    select new { Id = Id, Firstname = u.Firstname, Surname = u.Surname, Phone = u.PhoneNumber, Email = u.Email, Dealer = d != null ? d.ID : 0 , Branch = b != null ? b.ID : 0 }
                        ).Take(1);

                        result.result = user.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result.error = ex.Message;                
            }

            return result;
        }
        #endregion

    }

}