﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using static BellEquipment.Models.BellDbObject;

namespace BellEquipment.Models
{
    public class AuditModel
    {
        // Saved when a user logs into the application

        // Save when a user logs out of the application

        // Save when a user requests a Stats report email. 

        // AUDIT Table Structure
        // ID
        // User
        // Action (Login; Logout; ReportDownload)
        // Payload (Blob parameters)

        static string connString = System.Configuration.ConfigurationManager.ConnectionStrings["bellDBConnectionString"].ConnectionString.ToString();

        public enum AuditTypes
        {
            [Description("Login")]
            LoginToken,
            //Logout,
            [Description("StatisticsEmail")]
            StatisticsEmailDownloadRequest
        }

        public static async Task AuditCreateRecordAsync(Audit auditRecord)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        dc.Audit.InsertOnSubmit(auditRecord);
                        try
                        {
                            dc.SubmitChanges();
                        }
                        catch (Exception ex) { }
                    }
                }
            }
            catch (Exception e) { }
            //await Task.Delay(5000);
        }

        public static dbResult AuditRecords()
        {
            dbResult result = new dbResult() { result = new BellDbObject.Audit() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.Audit orderby x.ID descending select x).Take(25);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult AuditRecordByid(int AuditId)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Audit() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.Audit where x.ID == AuditId orderby x.ID descending select x).Take(25);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }
        public static dbResult AuditRecordByUser(string UserId)
        {
            dbResult result = new dbResult() { result = new BellDbObject.Audit() };
            try
            {
                // Query DB for complete list
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con))
                    {
                        var list = (from x in dc.Audit where x.UserId == UserId orderby x.ID descending select x).Take(25);
                        result.result = list.ToList();
                        result.success = true;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }
            return result;
        }

    }
}