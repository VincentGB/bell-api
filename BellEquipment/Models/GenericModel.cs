﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BellEquipment.Models
{
    public static class GenericModel
    {

        public static string GetObjectValueByName(Object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName).GetValue(obj, null).ToString();
        }
    }
}