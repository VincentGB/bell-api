﻿using BellEquipment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static BellEquipment.Models.BellDbObject;

namespace BellEquipment.Models
{
    public class UserDeviceTokenModel
    {
        static string connString = System.Configuration.ConfigurationManager.ConnectionStrings["bellDBConnectionString"].ConnectionString.ToString();

        public static dbResult SaveUserDeviceToken(string userId, string deviceToken, bool enableNotifications)
        {
            dbResult result = new dbResult() { result = new UserDeviceToken() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (BellEquipDataContext dc = new BellEquipDataContext(con))
                    {
                        var device = dc.UserDeviceTokens.Where(w => w.UserId.ToLower() == userId.ToLower()).FirstOrDefault();

                        if (ReferenceEquals(device, null))
                        {
                            device = new UserDeviceToken();
                            //CREATE
                            device.UserId = userId;
                            device.DeviceToken = deviceToken;
                            device.CreatedDate = DateTime.Now;
                            device.NotificationsIsEnabled = enableNotifications;

                            dc.UserDeviceTokens.InsertOnSubmit(device);

                            try
                            {
                                dc.SubmitChanges();
                                result.success = true;
                                result.result = device;
                            }
                            catch (Exception e)
                            {
                                result.success = false;
                                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), e.Message);
                            }
                        }
                        else
                        {
                            //UPDATE
                            device.DeviceToken = deviceToken;
                            device.DateLastModified = DateTime.Now;
                            device.NotificationsIsEnabled = enableNotifications;
                            try
                            {
                                dc.SubmitChanges();

                                result.success = true;
                                result.result = device;
                            }
                            catch (Exception e)
                            {
                                result.success = false;
                                result.error = string.Format("{0} : {1}", ErrorHandler.ValidationError.DatabaseRecordError.GetDescription(), e.Message);
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.error = ex.Message;
            }
            
            return result;
        }
        public static dbResult GetUserDeviceToken(string currentUserId)
        {
            dbResult result = new dbResult() { result = new UserDeviceToken() };
            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (BellEquipDataContext dc = new BellEquipDataContext(con))
                    {
                        result.result = dc.UserDeviceTokens.Where(w => w.UserId == currentUserId).FirstOrDefault();
                        result.success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result.error = ex.Message;
            }

            return result;
        }
        public static string[] GetUserDeviceTokens(string currentUserId)
        {
            List<string> result = new List<string>();

            try
            {
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (BellEquipDataContext dc = new BellEquipDataContext(con))
                    {
                        result = dc.UserDeviceTokens.Where(w => w.UserId != currentUserId && w.NotificationsIsEnabled).Select(s => s.DeviceToken).ToList();
                    }
                }
            }
            catch (Exception ex)
            {                                
            }

            return result.ToArray();
        }
    }
}