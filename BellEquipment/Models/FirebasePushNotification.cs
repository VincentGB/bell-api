﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BellEquipment.Models
{
    public class FirebasePushNotification
    {
        private static Uri FireBasePushNotificationsURL = new Uri("https://fcm.googleapis.com/fcm/send");
        private static string ServerKey = "AAAAthnXtCI:APA91bHuwFdNo5XAg-8mm16uZ-gUBkCX-clDjJGLnTMR9cYN3S6ehlOhdKCnnxZo8c-MKXfNeHIO9dnRWCUAGGu8K25ajOhH4GYpniURv8qGCM3QBfDxrubTTDpxWc86yAyP-NxxljGg";

        public static async Task<bool> SendPushNotification(int notificationId, string userId)
        {
            bool sent = false;

            var deviceTokens = UserDeviceTokenModel.GetUserDeviceTokens(userId);
            //buld message
            var notification = NotificationModel.NotificationById(notificationId);

            if (notification.result != null)
            {

                var messageInformation = new Message()
                {
                    notification = new Notification
                    {
                        title = "",
                        text = ""
                    },
                    data = notification.result,
                    registration_ids = deviceTokens
                };

                if (deviceTokens.Count() > 0)
                {
                    string reportid = GenericModel.GetObjectValueByName(notification.result, "ReportId");
                    string reportnumber = GenericModel.GetObjectValueByName(notification.result, "ReportNumber");
                    string actionType = GenericModel.GetObjectValueByName(notification.result, "ActionType");
                    string userName = GenericModel.GetObjectValueByName(notification.result, "UserName");
                    string createdDate = GenericModel.GetObjectValueByName(notification.result, "CreatedDate");

                    switch (actionType.ToLower())
                    {
                        case "message":
                            messageInformation.notification.title = "New Message";
                            messageInformation.notification.text = string.Format("{0} added a new message to report {1}", userName, reportnumber);
                            break;
                        case "reportclosed":
                            messageInformation.notification.title = "Report Closed";
                            messageInformation.notification.text = string.Format("{0} has closed report {1}", userName, reportnumber);
                            break;
                        case "reportcreated":
                            messageInformation.notification.title = "New Report Created";
                            messageInformation.notification.text = string.Format("{0} added a new report {1}", userName, reportnumber);
                            break;
                        case "reportupdated":
                            messageInformation.notification.title = "Report Updated";
                            messageInformation.notification.text = string.Format("{0} updated report {1}", userName, reportnumber);
                            break;
                        default:
                            break;
                    }                    

                    string jsonMessage = JsonConvert.SerializeObject(messageInformation);

                    var request = new HttpRequestMessage(HttpMethod.Post, FireBasePushNotificationsURL);

                    request.Headers.TryAddWithoutValidation("Authorization", "key=" + ServerKey);
                    request.Content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");

                    HttpResponseMessage result;
                    using (var client = new HttpClient())
                    {
                        result = await client.SendAsync(request);
                        sent = sent && result.IsSuccessStatusCode;
                    }
                }
            }
           
            return sent;
        }

        private class Message
        {
            public string[] registration_ids { get; set; }
            public Notification notification { get; set; }
            public object data { get; set; }
        }

        private class Notification
        {
            public string title { get; set; }
            public string text { get; set; }
        }        
    }
}