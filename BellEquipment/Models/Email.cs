﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace BellEquipment.Models
{
    public class Email
    {
        public static bool SendEmail(List<string> to, string subject, string body, Attachment docs = null)
        {
            try
            {
                SmtpClient client = smtpClientDetails();
                MailMessage mm = new MailMessage(); //("no-reply@mashlab.co.za", to, "You have a new message", body)
                mm.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["smtpEmailDefaultFromEmail"]);

                //mm.To.ToList().ForEach(t => mm.To.Add(t));
                foreach (string t in to)
                {
                    if (!string.IsNullOrEmpty(t))
                    {
                        mm.To.Add(t);
                    }
                }

                if (docs != null)
                    mm.Attachments.Add(docs);

                mm.Subject = subject;
                mm.Body = body;
                mm.IsBodyHtml = true;
                client.Send(mm);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        internal static SmtpClient smtpClientDetails()
        {
            SmtpClient client = new SmtpClient();
            client.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpEmailPort"]);
            client.Host = System.Configuration.ConfigurationManager.AppSettings["smtpEmailHost"];
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;




            return client;
        }
    }
}