﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static BellEquipment.Models.BellDbObject;

namespace BellEquipment.Models
{
    public class InternalModel : dbResult
    {
        static string connString = System.Configuration.ConfigurationManager.ConnectionStrings["bellDBConnectionString"].ConnectionString.ToString();

        public static void LinkUserToBranch(string UserId, int BranchId)
        {
            dbResult result = new dbResult() { result = new object() };
            try
            {
                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connString))
                {
                    using (BellEquipDataContext ctx = new BellEquipDataContext(conn))
                    {                        
                        var branch = new UserBranch();
                        branch.BranchId = BranchId;
                        branch.UserId = UserId;

                        ctx.UserBranch.InsertOnSubmit(branch);
                        ctx.SubmitChanges();
                    }

                    result.success = true;
                }
            }
            catch (Exception ex)
            {
                result.error = ex.Message;
            }
        }
    }
}