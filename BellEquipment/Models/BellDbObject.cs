﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BellEquipment.Models
{
    public class BellDbObject
    {
        public class dbResult
        {
            public object result { get; set; }
            public bool success { get; set; }
            public object error { get; set; }
        }

        #region Table Classes
        [Table(Name = "Configuration")]
        public class ConfigurationTable
        {
            [Column(Name = "Name", CanBeNull = false)]
            public string ConfigName;
            [Column(Name = "Value", CanBeNull = false)]
            public string ConfigValue;
        }

        [Table(Name = "AspNetUsers")]
        public class Users
        {
            [Column(Name = "Id", IsPrimaryKey = true)]
            public string ID;
            [Column(Name = "Firstname", CanBeNull = false)]
            public string Firstname;
            [Column(Name = "Surname", CanBeNull = false)]
            public string Surname;
            [Column(Name = "PhoneNumber")]
            public string PhoneNumber;
            [Column(Name = "Email")]
            public string Email;
            [Column(Name = "EmailNotificationsEnabled", CanBeNull = false)]
            public bool EmailNotificationsEnabled;
        }

        [Table(Name = "Dealers")]
        public class Dealers
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Name", CanBeNull = false)]
            public string Name;
            [Column(Name = "MobileNumber")]
            public string MobileNumber;
            [Column(Name = "Email")]
            public string Email;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }

        [Table(Name = "Branch")]
        public class Branch
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Name", CanBeNull = false)]
            public string Name;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }

        [Table(Name = "UserBranch")]
        public class UserBranch
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "UserId", CanBeNull = false)]
            public string UserId;
            [Column(Name = "BranchId", CanBeNull = false)]
            public int BranchId;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }
        [Table(Name = "DealerBranch")]
        public class DealerBranch
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "DealersId", CanBeNull = false)]
            public int DealersId;
            [Column(Name = "BranchId", CanBeNull = false)]
            public int BranchId;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }

        [Table(Name = "ModelsCategory")]
        public class ModelsCategory
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }

        [Table(Name = "Models")]
        public class Models
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;

            private int _ModelsId;
            private EntityRef<ModelsCategory> _ModelsCategory;

            [Column(Storage = "_ModelsId", DbType = "INT")]
            public int ModelsCategoryId
            {
                get { return this._ModelsId; }
                set { this._ModelsId = value; }
            }

            [Association(Name = "_ModelsCategory", ThisKey = "ModelsCategoryId")]
            //[Column(Name = "ModelsCategoryId", CanBeNull = false)]
            public ModelsCategory ModelsCategory
            {
                get { return this._ModelsCategory.Entity; }
                set { this._ModelsCategory.Entity = value; }
            }

            //[Column(Name = "ModelsCategoryId", CanBeNull = false)]
            //public string ModelsCategoryId;

            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
            //}
        }

        [Table(Name = "NonStandardAddons")]
        public class NonStandardAddons
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;
            [Column(Name = "ModelsCategoryId", CanBeNull = false)]
            public int ModelsCategoryId;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }

        [Table(Name = "FaultCode")]
        public class FaultCode
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }
        [Table(Name = "FaultSubCode")]
        public class FaultSubCode
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;
            [Column(Name = "FaultCodeId", CanBeNull = false)]
            public int FaultCodeId;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }

        [Table(Name = "Application")]
        public class Application
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int Id;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }
        [Table(Name = "Complaint")]
        public class Complaint
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }
        [Table(Name = "Defect")]
        public class Defect
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }

        [Table(Name = "Report")]
        public class Report
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int? ID;
            [Column(Name = "ReportNumber", CanBeNull = false)]
            public string ReportNumber;
            [Column(Name = "ReportDate", CanBeNull = false)]
            [Newtonsoft.Json.JsonProperty(ItemConverterType = typeof(Newtonsoft.Json.Converters.JavaScriptDateTimeConverter))]
            public DateTime ReportDate;
            [Column(Name = "ReportPersonId")]
            public string ReportPersonId;
            [Column(Name = "DealersId")]
            public int? DealersId;
            [Column(Name = "Customer")]
            public string Customer;
            [Column(Name = "VinNo")]
            public string VinNo;
            [Column(Name = "ModelId")]
            public int? ModelId;
            [Column(Name = "ReportTypeId")]
            public int? ReportTypeId;
            [Column(Name = "ReportedToId")]
            public string ReportedToId;
            [Column(Name = "FaultCodeId")]
            public int? FaultCodeId;
            [Column(Name = "FaultSubCodeId")]
            public int? FaultSubCodeId;
            [Column(Name = "NonStandardAddonsId")]
            public int? NonStandardAddonsId;
            [Column(Name = "FaultCodeReported")]
            public string FaultCodeReported;
            [Column(Name = "FailedPartNumber")]
            public string FailedPartNumber;
            [Column(Name = "ReplacementPartNumber")]
            public string ReplacementPartNumber;
            [Column(Name = "ComplaintId")]
            public int? ComplaintId;
            [Column(Name = "DefectId")]
            public int? DefectId;
            [Column(Name = "ComplaintDescription")]
            public string ComplaintDescription;
            [Column(Name = "FailedComponentSN")]
            public string FailedComponentSN;
            [Column(Name = "RootCause")]
            public string RootCause;
            [Column(Name = "ActionRequired")]
            public string ActionRequired;

            [Column(Name = "SafetyHard", CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
            public bool? SafetyHard;

            //[Column(Name = "ApplicaitonId", CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
            [Column(Name = "ApplicaitonId")]
            public int? ApplicaitonId;
            [Column(Name = "Hours", DbType = "numeric(18,2)")]
            public decimal? Hours;
            [Column(Name = "Status", CanBeNull = false)]
            public int Status;
            //[Column(Name = "CreatedBy", CanBeNull = false)]
            //public string CreatedBy = "System";
            //[Column(Name = "CreatedDate", IsDbGenerated = true, CanBeNull = false)]
            //public DateTime CreatedDate;
        }

        [Table(Name = "Report")]
        public class ReportCreatedBy
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int ID;
            [Column(Name = "CreatedBy")]
            public string CreatedBy;
        }

        [Table(Name = "ReportStatus")]
        public class ReportStatus
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }

        [Table(Name ="ReportType")]
        public class ReportType
        {
            [Column(Name = "ID", IsPrimaryKey = true)]
            public int ID;
            [Column(Name = "Title", CanBeNull = false)]
            public string Title;
            [Column(Name = "Status", CanBeNull = false)]
            public bool Status;
        }

        [Table(Name = "Messages")]
        public class Messages
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int? ID;
            [Column(Name = "Post", CanBeNull = false)]
            public string Post;
            [Column(Name = "IsDocument", CanBeNull = false)]
            public bool IsDocument;
            [Column(Name = "CreatedDate", CanBeNull = false, IsDbGenerated = true)]
            public DateTime CreatedDate;
        }
        [Table(Name = "ReportMessages")]
        public class ReportMessages
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int ID;
            [Column(Name = "ReportId", CanBeNull = false)]
            public int ReportId;
            [Column(Name = "UserId", CanBeNull = false)]
            public string UserId;
            [Column(Name = "MessageId", CanBeNull = false)]
            public int MessageId;
        }

        [Table(Name = "Document")]
        public class DocumentTable
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int ID;
            [Column(Name = "Extension", CanBeNull = false)]
            public string Extension;
            [Column(Name = "FileName", CanBeNull = false)]
            public string FileName;
            [Column(Name = "DocumentContent", CanBeNull = false)]
            public string DocumentContent;
            [Column(Name = "CreatedBy", CanBeNull = true)]
            public string CreatedBy;
            [Column(Name = "CreatedDate", CanBeNull = true)]
            public DateTime CreatedDate;
        }

        [Table(Name = "ReportDocuments")]
        public class ReportDocuments
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int ID;
            [Column(Name = "ReportId", CanBeNull = false)]
            public int ReportId;
            [Column(Name = "DocumentId", CanBeNull = false)]
            public int DocumentId;
            [Column(Name = "IsRemoved")]
            public bool IsRemoved;
        }

        [Table(Name = "MessageDocuments")]
        public class MessageDocuments
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int ID;
            [Column(Name = "MessageId", CanBeNull = false)]
            public int MessageId;
            [Column(Name = "DocumentId", CanBeNull = false)]
            public int DocumentId;
            [Column(Name = "IsRemoved")]
            public bool IsRemoved;
        }

        [Table(Name = "Notification")]
        public class Notification
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int ID;
            [Column(Name = "ReportId", CanBeNull = false)]
            public int ReportId;
            [Column(Name = "ActionType", CanBeNull = false)]
            public string ActionType;
            [Column(Name = "ActionId", CanBeNull = false)]
            public int? ActionId;
            [Column(Name = "CreatedBy", CanBeNull = true)]
            public string CreatedBy;
            [Column(Name = "CreatedDate", CanBeNull = true)]
            public DateTime CreatedDate;
        }
        
        [Table(Name = "Audit")]
        public class Audit
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int ID;
            [Column(Name = "UserId", CanBeNull = false)]
            public string UserId;
            [Column(Name = "ActionType", CanBeNull = false)]
            public string ActionType;
            [Column(Name = "Payload", CanBeNull = false)]
            public string Payload;
            [Column(Name = "CreatedDate", CanBeNull = true)]
            public DateTime CreatedDate;
        }

        [Table(Name = "UserDeviceToken")]
        public class UserDeviceToken
        {
            [Column(Name = "ID", IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false)]
            public int ID;
            [Column(Name = "UserId", CanBeNull = false)]
            public string UserId;
            [Column(Name = "DeviceToken", CanBeNull = false)]
            public string DeviceToken;
            [Column(Name = "CreatedDate", CanBeNull = false)]
            public DateTime CreatedDate;
            [Column(Name = "DateLastModified", CanBeNull = true)]
            public DateTime? DateLastModified;
            [Column(Name = "NotificationsIsEnabled", CanBeNull = false)]
            public bool NotificationsIsEnabled;
        }

        #endregion

        public class BellEquipDataContext : DataContext
        {
            // Inherit from the DataContext class.
            // One constructor accepts an SqlConnection, or we can pass the connectionstring directly.
            public BellEquipDataContext(string cs) : base(cs) { }
            public BellEquipDataContext(SqlConnection con) : base(con) { }

            // Create the tables for the database (datacontext)
            public Table<ConfigurationTable> ConfigurationTable;

            public Table<Users> Users;
            public Table<Dealers> Dealers;

            public Table<Branch> Branch;
            public Table<UserBranch> UserBranch;
            public Table<DealerBranch> DealerBranch;

            public Table<FaultCode> FaultCode;
            public Table<FaultSubCode> FaultSubCode;

            public Table<ModelsCategory> ModelsCategory;
            public Table<Models> Models;
            public Table<NonStandardAddons> NonStandardAddons;

            public Table<Application> Application;
            public Table<Complaint> Complaint;
            public Table<Defect> Defect;

            public Table<Report> Reports;
            public Table<ReportCreatedBy> ReportCreatedBy;
            public Table<ReportStatus> ReportStatus;
            public Table<ReportType> ReportType;

            public Table<Messages> Messages;
            public Table<ReportMessages> ReportMessages;
            public Table<MessageDocuments> MessageDocuments;

            public Table<DocumentTable> DocumentTable;
            public Table<ReportDocuments> ReportDocuments;

            public Table<Notification> Notification;
            public Table<Audit> Audit;

            public Table<UserDeviceToken> UserDeviceTokens;
        }

    }
}