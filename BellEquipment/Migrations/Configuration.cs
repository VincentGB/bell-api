namespace BellEquipment.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BellEquipment.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "BellEquipment.Models.ApplicationDbContext";
        }

        protected override void Seed(BellEquipment.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Users.AddOrUpdate(
                p => p.UserName,
                new Models.ApplicationUser { UserName = "Admin@bell.co.za", Email = "Admin@bell.co.za", Firstname = "Admin", Surname = "Bell" },
                new Models.ApplicationUser { UserName = "vincent@mashlab.co.za", Email = "vincent@mashlab.co.za", Firstname = "Vincent", Surname = "Blasl", PhoneNumber = "0825628415" },
                new Models.ApplicationUser { UserName = "Robbert.geach@za.bellequipment.com", Email = "Robbert.geach@za.bellequipment.com", Firstname = "Robbie", Surname = "Geach", PhoneNumber = "0828248507" },
                new Models.ApplicationUser { UserName = "Meltus.badenhorst@za.bellequipment.com", Email = "Meltus.badenhorst@za.bellequipment.com", Firstname = "Meltus", Surname = "Badenhorst", PhoneNumber = "0823780277" },
                new Models.ApplicationUser { UserName = "pieter.neethling@za.bellequipment.com", Email = "pieter.neethling@za.bellequipment.com", Firstname = "Pieter", Surname = "Neethling", PhoneNumber = "0824187666" },
                new Models.ApplicationUser { UserName = "ryanch@bell.co.za", Email = "ryanch@bell.co.za", Firstname = "Ryan", Surname = "Chase", PhoneNumber = "0823781472" },
                new Models.ApplicationUser { UserName = "Ianmc@bell.co.za", Email = "Ianmc@bell.co.za", Firstname = "Ian", Surname = "Mcleod", PhoneNumber = "0823009538" },
                new Models.ApplicationUser { UserName = "Lionelb@bell.co.za", Email = "Lionelb@bell.co.za", Firstname = "Lionel", Surname = "Bester", PhoneNumber = "0828550362" },
                new Models.ApplicationUser { UserName = "Gertv@bell.co.za", Email = "Gertv@bell.co.za", Firstname = "Gert", Surname = "Venter", PhoneNumber = "0795073090" },
                new Models.ApplicationUser { UserName = "Neilr@uk.bellequipment.com", Email = "Neilr@uk.bellequipment.com", Firstname = "Neil", Surname = "Robson", PhoneNumber = "+44 (0) 78 796 26319" },
                new Models.ApplicationUser { UserName = "Todd.a@bipond.com", Email = "Todd.a@bipond.com", Firstname = "Todd", Surname = "Alexander", PhoneNumber = "+61 (0) 41 789 4422" },
                new Models.ApplicationUser { UserName = "ryanc@bellequipment.com", Email = "ryanc@bellequipment.com", Firstname = "Ryan", Surname = "Castle", PhoneNumber = "+1 (0) 980 333 5159" },
                new Models.ApplicationUser { UserName = "Brian.Pleinis@us.bellequipment.com", Email = "Brian.Pleinis@us.bellequipment.com", Firstname = "Brian", Surname = "Pleinis", PhoneNumber = "+1 (0) 678 938 0279" },
                new Models.ApplicationUser { UserName = "sakkied@bell.co.za", Email = "sakkied@bell.co.za", Firstname = "Sakkie", Surname = "Du Plessis", PhoneNumber = "0823771479" },
                new Models.ApplicationUser { UserName = "petrie.dutoit@za.bellequipment.com", Email = "petrie.dutoit@za.bellequipment.com", Firstname = "Petrie", Surname = "Du Toit", PhoneNumber = "0824432300" },
                new Models.ApplicationUser { UserName = "wayneh@bell.co.za", Email = "wayneh@bell.co.za", Firstname = "Wayne", Surname = "Hajee", PhoneNumber = "0829270610" },
                new Models.ApplicationUser { UserName = "geoffj@bell.co.za", Email = "geoffj@bell.co.za", Firstname = "Geoff", Surname = "Jacklin", PhoneNumber = "0829716927" },
                new Models.ApplicationUser { UserName = "jamesf@bell.co.za", Email = "jamesf@bell.co.za", Firstname = "James", Surname = "Fleetwood", PhoneNumber = "0609710700" },
                new Models.ApplicationUser { UserName = "Seelans@bell.co.za", Email = "Seelans@bell.co.za", Firstname = "Seelan", Surname = "Singh", PhoneNumber = "0726568065" },
                new Models.ApplicationUser { UserName = "freddieg@bell.co.za", Email = "freddieg@bell.co.za", Firstname = "Freddie", Surname = "Grobler", PhoneNumber = "0845484688" },
                new Models.ApplicationUser { UserName = "garethc@bell.co.za", Email = "garethc@bell.co.za", Firstname = "Gareth", Surname = "Campbell", PhoneNumber = "0716732476" },
                new Models.ApplicationUser { UserName = "ryanb@bell.co.za", Email = "ryanb@bell.co.za", Firstname = "Ryan", Surname = "Burton", PhoneNumber = "0713518380" },
                new Models.ApplicationUser { UserName = "Iainb@bell.co.za", Email = "Iainb@bell.co.za", Firstname = "Iain", Surname = "Beattie", PhoneNumber = "0823009514" },
                new Models.ApplicationUser { UserName = "Ruzaanl@bell.co.za", Email = "Ruzaanl@bell.co.za", Firstname = "Ruzaan", Surname = "Labuschagne", PhoneNumber = "0720496554" },
                new Models.ApplicationUser { UserName = "ryan.campbell@za.bellequipment.com", Email = "ryan.campbell@za.bellequipment.com", Firstname = "Ryan", Surname = "Campbell", PhoneNumber = "0716268636" }
                );
        }
    }
}
