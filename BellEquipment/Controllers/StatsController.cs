﻿using BellEquipment.Common;
using System;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace BellEquipment.Controllers
{
    [Authorize]
    public class StatsController : ApiController
    {
        #region Statistics
        public class stats
        {
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
        }
        [HttpPost()]
        [Route("api/Stats/")]
        public IHttpActionResult StatsByUser([System.Web.Http.FromBody] stats s)
        {
            // Return Open + Closed + Total reports for Current Month 
            if (s != null)
            {
                // Utils.ToDateFromStringDDMMYYYY(s.StartDate)
                if (s.StartDate != null && s.EndDate != null && s.StartDate != DateTime.MinValue && s.EndDate != DateTime.MinValue)
                {
                    return Ok(Models.ReportModel.StatsByUserPeriod(User.Identity.GetUserId(), s.StartDate, s.EndDate));
                }
            }
            return Ok(Models.ReportModel.StatsByUserId(User.Identity.GetUserId(), DateTime.Now));
            //return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpPost()]
        public IHttpActionResult StatsReportDownload(stats s)
        {
            // Returns SOMETHING 
            Models.AuditModel.AuditCreateRecordAsync(new Models.BellDbObject.Audit() { UserId = User.Identity.GetUserId(), ActionType = Models.AuditModel.AuditTypes.StatisticsEmailDownloadRequest.GetDescription(), Payload = Newtonsoft.Json.JsonConvert.SerializeObject(s), CreatedDate = DateTime.Now  });

            return Ok(new Models.BellDbObject.dbResult() { success = false, error = new List<string> { Common.ErrorHandler.ValidationError.FunctionalityDoesNotExist.GetDescription() } });
        }
        #endregion
    }
}
