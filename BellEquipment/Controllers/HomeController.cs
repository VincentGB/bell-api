﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;

namespace BellEquipment.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }

        [Route("About")]
        [Route("Home/About")]
        //[Authorize]
        public ActionResult About()
        {
            return View();
        }

        [Route("Home/EmailPDF")]
        public ActionResult EmailPDF()
        {
            ////var pdfResult = new Rotativa.ViewAsPdf("about") { FileName = "Report.pdf" };
            ////byte[] applicationPDFData = pdfResult.BuildPdf(ControllerContext);
            ////System.IO.MemoryStream file = new System.IO.MemoryStream(applicationPDFData);
            ////file.Seek(0, System.IO.SeekOrigin.Begin);
            ////System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(file,"test.pdf", "application/pdf");

            ////Models.Email.SendEmail(
            ////    new List<string>{"vincentgblasl@gmail.com"}, 
            ////    "New Report Created",
            ////    "Hello,<br/> <p>There has been a new report logged.</p><p></p> Regards, <br/>Team Mashlab <br/><br/>We need to define the mail body",
            ////    attachment
            ////    );
            ////return new Rotativa.PartialViewAsPdf("about");

            ////return new Rotativa.PartialViewAsPdf("ReportPDF", Models.ReportModel.ReportGenerateView("1"));
            ////return View("ReportPDF", Models.ReportModel.ReportGenerateView("1"));
            //Models.ReportModel.ReportGenerateEmail(this.ControllerContext, "1");

            return View("About");
        }

        //[Route("UserDetail")]
        [Route("Home/UserDetail")]
        [Authorize]
        public ActionResult Detail()
        {
            string id;
            if (User != null && User.Identity != null && User.Identity.GetUserId() != null)
            {
                id = User.Identity.GetUserId();
                var c = new Models.ApplicationDbContext();
                var u = c.Users.Where(x => x.Id == id).Select(x => new { x.Id, x.Firstname, x.Surname, x.Avatar, x.LockoutEnabled }).ToList();
                ViewBag.Details = string.Format("ID: {0} | UserData: {1}", id, Newtonsoft.Json.JsonConvert.SerializeObject(u));
            }
            else
            {
                var c = new Models.ApplicationDbContext();
                var u = c.Users.Select(x => new { x.Id, x.Firstname, x.Surname, x.Avatar, x.LockoutEnabled }).ToList();
                ViewBag.Details = string.Format("UserData: {0}", Newtonsoft.Json.JsonConvert.SerializeObject(u));
            }

            return View();
        }

        //[Route("UserProfile")]
        [Route("Home/UserProfile")]
        [Authorize]
        public JsonResult UserProfile()
        {
            string id;
            if (User != null && User.Identity != null && User.Identity.GetUserId() != null)
            {
                id = User.Identity.GetUserId();
                var c = new Models.ApplicationDbContext();
                var u = c.Users.Where(x => x.Id == id).Select(x => new { x.Id, x.Firstname, x.Surname, x.Avatar, x.LockoutEnabled }).ToList();
                return Json(u, JsonRequestBehavior.AllowGet);
            }
            return Json(null);
        }

    }
}
