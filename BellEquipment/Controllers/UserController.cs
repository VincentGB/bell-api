﻿using BellEquipment.Common;
using System;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace BellEquipment.Controllers
{
    [Authorize]
    public class UserController : ApiController
    {
        #region User
        //// GET: http://localhost:62893/api/User/xxxxxxxx
        [Route("api/User/")]
        [Route("api/User/{id?}")]
        //[Route("User/{id:}")]
        [HttpGet()]
        public IHttpActionResult Users(string id = null)
        {
            if (!String.IsNullOrEmpty(id))
            {
                return Ok(Models.ReportModel.User(id));
            }
            else if (User.Identity.IsAuthenticated)
            {
                return Ok(Models.ReportModel.User(User.Identity.GetUserId()));
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }
        //// GET: http://localhost:62893/api/User/List
        [Route("api/User/List")]
        [HttpGet()]
        public IHttpActionResult UserList()
        {
            return Ok(Models.ReportModel.UserList());
        }

        [Route("api/User/SetEmailNotificationStatus")]
        [HttpGet()]
        public IHttpActionResult SetEmailNotificationStatus()
        {
            return Ok(Models.ReportModel.UserList());
        }
        #endregion

        #region Dealer
        [Route("api/Dealer/{id?}")]
        [HttpGet()]
        public IHttpActionResult Dealer(string id)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.Dealer(Convert.ToInt32(id)));
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }
        //// GET: http://localhost:62893/api/User/List
        [Route("api/Dealer/List")]
        [HttpGet()]
        public IHttpActionResult DealerList()
        {
            return Ok(Models.ReportModel.DealerList());
        }
        #endregion
    }

}
