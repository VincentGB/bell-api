﻿using BellEquipment.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BellEquipment.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/internal")]
    public class InternalController : ApiController
    {
        private ApplicationUserManager _userManager;

        public InternalController()
        {

        }

        public InternalController(ApplicationUserManager userManager)
        {
            UserManager = userManager;            
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpGet]
        public IHttpActionResult UserList()
        {
            //GET All Current FIR APP USERS
            return Ok(Models.ReportModel.InternalUserList());
        }

        [HttpGet]
        [Route("GetUser/{Id}")]
        public IHttpActionResult GetUserById(string Id)
        {
            //GET THE USER DATA
            var user = Models.ReportModel.InternalUser(Id);
            return Ok(user);
        }

        [HttpPost]
        [Route("CreateUser")]
        //CREATE USER
        public async Task<IHttpActionResult> InternalCreateUser(InternalUserModel model)
        {
            var user = new ApplicationUser()
            {
                UserName = model.Email,                
                Email = model.Email,
                PhoneNumber = model.MobileNumber,
                Firstname = model.Firstname,
                Surname = model.Surname
            };

            var userResult = await UserManager.CreateAsync(user);

            if (userResult.Succeeded)
            {
                //CREATE UserBranch Link
                InternalModel.LinkUserToBranch(user.Id, model.BranchId);
            }

            return Ok(userResult);
        }

        [HttpPut]
        [Route("UpdateUser")]
        public async Task<IHttpActionResult> InternalUpdateUser(InternalUserModel model)
        {
            var currentUserData = UserManager.FindByEmail(model.Email);

            currentUserData.Firstname = model.Firstname;
            currentUserData.PhoneNumber = model.MobileNumber;
            currentUserData.Surname = model.Surname;

            var updateResult = await UserManager.UpdateAsync(currentUserData);

            return Ok(updateResult);
        }
    }
}
