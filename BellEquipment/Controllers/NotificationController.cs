﻿using BellEquipment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
// ----------------
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Web;
using BellEquipment.Models;

namespace BellEquipment.Controllers
{
    //[Authorize]
    public class NotificationController : ApiController
    {
        private static ConcurrentBag<StreamWriter> clients;

        static NotificationController()
        {
            clients = new ConcurrentBag<StreamWriter>();
        }

        [HttpGet()]
        //// GET: http://localhost:62893/api/notification/ 
        [Route("api/notification/{userId}")]
        [Route("api/notification/{id:int}")]
        [Route("api/notification/")]
        public IHttpActionResult Notifications(int? id = null, string userId = null)
        {
            if (id != null)
                return Ok(Models.NotificationModel.NotificationsByid(Convert.ToInt32(id)));
            else if (!string.IsNullOrEmpty(userId))
                return Ok(Models.NotificationModel.NotificationsByUser(userId));
            return Ok(Models.NotificationModel.Notifications());
        }

        //[HttpGet()]
        //[Route("api/notification/subscribe")]
        //[AllowAnonymous]
        //public HttpResponseMessage Subscribe(HttpRequestMessage request)
        //{
        //    var response = request.CreateResponse();
        //    response.Content = new PushStreamContent((a, b, c) =>
        //    { OnStreamAvailable(a, b, c); }, "text/event-stream");
        //    return response;
        //}

        private void OnStreamAvailable(Stream stream, HttpContent content,
            TransportContext context)
        {
            var client = new StreamWriter(stream);
            clients.Add(client);
        }

        public async Task NotificationCallBackMessage(int notificationId)
        {
            var notification = Models.NotificationModel.NotificationById(notificationId);

            if (notification.error == null)
            {
                //var notification = string.Format("You have a new message");
                foreach (var client in clients)
                {
                    try
                    {
                        string reportid = GenericModel.GetObjectValueByName(notification.result, "ReportId");
                        string reportnumber = GenericModel.GetObjectValueByName(notification.result, "ReportNumber");
                        string actionType = GenericModel.GetObjectValueByName(notification.result, "ActionType");
                        string userName = GenericModel.GetObjectValueByName(notification.result, "UserName");
                        string createdDate = GenericModel.GetObjectValueByName(notification.result, "CreatedDate");
                        //STRUCTURE OF data: {} 0 = report id, 1 = reportnumber, 2 = action type, 3 = username, 4 = created date
                        var data = string.Format("data: {0}|{1}|{2}|{3}|{4}\n\n", reportid, reportnumber, actionType, userName, createdDate);
                        await client.WriteAsync(data);
                        await client.FlushAsync();
                        client.Dispose();
                    }
                    catch (Exception ex)
                    {
                        StreamWriter ignore;
                        clients.TryTake(out ignore);
                    }
                }
            }
        }

        #region Firebase Push Notification Methods
        [HttpGet]
        [Route("api/notifications/getnotificationsstatus/")]
        public IHttpActionResult GetNotificationsStatus(string userId)
        {
            return Ok(UserDeviceTokenModel.GetUserDeviceToken(userId));
        }
        [HttpPost]
        [Route("api/notifications/subscribe/")]        
        public IHttpActionResult Subscribe([FromBody]PushNotificationSubscribe Payload)
        {
            return Ok(UserDeviceTokenModel.SaveUserDeviceToken(Payload.UserId, Payload.DeviceToken,true));            
        }
        [HttpPost]
        [Route("api/notifications/unsubscribe/")]
        public IHttpActionResult Unsubscribe([FromBody]PushNotificationSubscribe Payload)
        {
            return Ok(UserDeviceTokenModel.SaveUserDeviceToken(Payload.UserId, Payload.DeviceToken, false));
        }
        #endregion

        /*
         * Create
         * Save 
         * Retrieval
         * 
         * 
         * 
         * Push Notifications
         * 
         * 
         * */


    }
}
