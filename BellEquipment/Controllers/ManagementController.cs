﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace BellEquipment.Controllers
{
    public class ManagementController : ApiController
    {
        // POST: http://localhost:62893/api/Management/postFunction
        /* // BODY - Content-Type | application/json
        {
            "FirstName": "asdasdas",
            "LastName": "qweqwe",
            "Age": 12
        }
        */
        [HttpGet()]
        //[Route("postFunction")]
        public void postFunction([System.Web.Http.FromBody] Person p)
        {
            //return Ok(new { success = true, result = string.Format("{0} {1} {2}", p.FirstName, p.LastName, p.Age) });
        }
        [HttpPost()]
        //[Route("PostResponse")]
        public IHttpActionResult PostResponse([System.Web.Http.FromBody] Person p)
        {
            var item = new { success = true, result = string.Format("p {0} {1} {2}", p.FirstName, p.LastName, p.Age) };
            //return Json(item);
            return Ok(item);
        }

        [HttpPost()]
        //[Route("PostResponse")]
        public IHttpActionResult PostTo([System.Web.Http.FromBody] string FirstName)
        {
            var item = new { success = true, result = string.Format("FirstName {0}", FirstName) };
            //return Json(item);
            return Ok(item);
        }
    }

    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
