﻿using BellEquipment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
// ----------------
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Web;

namespace BellEquipment.Controllers
{
        [Authorize]
    public class MessagesController : ApiController
    {
        [HttpGet()]
        //// GET: http://localhost:62893/api/messages/report/ | ID = null
        [Route("api/messages/report")]
        //// GET: http://localhost:62893/api/messages/report/1 | ID = 1
        [Route("api/messages/report/{id?}")]
        //[Route("Report")]
        public IHttpActionResult ReportMessages(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.MessagesByReportId(Convert.ToInt32(id)));
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        public class newMessage
        {
            public int ReportId;
            public string Post;
        }
        [HttpPost()]
        //// GET: http://localhost:62893/api/messages/report/ | ID = null
        [Route("api/messages/report")]
        public IHttpActionResult PostMessage([System.Web.Http.FromBody] newMessage msg)//([System.Web.Http.FromBody] string name)
        {
            if (User != null && User.Identity.IsAuthenticated &&  msg != new newMessage() && Utils.CheckIsNumeric(msg.ReportId.ToString()) && !string.IsNullOrEmpty(msg.Post))
            {
                return Ok(Models.ReportModel.MessageSavePost(msg.ReportId, User.Identity.GetUserId(), msg.Post));
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpGet()]
        [Route("api/messages/Document/{id:int}")]
        public IHttpActionResult DocumentsByReport(int? id = null)
        {
            if (!string.IsNullOrEmpty(id.ToString()) && Utils.CheckIsNumeric(id.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.MessageDocumentDetail((int)id);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpPost()]
        [Route("api/messages/document")]
        public IHttpActionResult PostMessageDocument()
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Form != null && HttpContext.Current.Request.Form.Count > 0
                && !string.IsNullOrEmpty(HttpContext.Current.Request.Form["ReportID"]) && Utils.CheckIsNumeric(HttpContext.Current.Request.Form["ReportID"])
                && User.Identity.IsAuthenticated)
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    List<HttpPostedFile> postedFiles = new List<HttpPostedFile>();
                    for (int i = 0; i < HttpContext.Current.Request.Files.Count; i++)
                    {
                        postedFiles.Add(HttpContext.Current.Request.Files[i]);
                    }
                    return Ok(Models.ReportModel.MessageSaveDocument(Convert.ToInt32(HttpContext.Current.Request.Form["ReportID"]), User.Identity.GetUserId(), postedFiles));
                }
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpPost()]
        [Route("api/messages/Document/SaveImage")]
        public IHttpActionResult DocumentsSaveBase64String([System.Web.Http.FromBody] Models.ReportModel.DocListBase64String docList)
        {
            if (docList != null && docList != new Models.ReportModel.DocListBase64String()
                && docList.ReportId > 0 && docList.Docs.Any())
            {
                return Ok(Models.ReportModel.MessageSaveDocumentBase64(docList.ReportId, User.Identity.GetUserId(), docList.Docs));
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        //    // ----------------------------------
        //    // Push Notification In Web Application Using Web API And PushContentStream
        //    // https://www.c-sharpcorner.com/article/push-notification-in-web-application-using-web-api-and-pushcontentstream/
        //    private static ConcurrentBag<StreamWriter> clients;
        //    static MessagesController()
        //    {
        //        clients = new ConcurrentBag<StreamWriter>();
        //    }

        //    public async Task PostAsync(ChatMessage m)
        //    {
        //        m.dt = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
        //        await ChatCallbackMsg(m);
        //    }
        //    private async Task ChatCallbackMsg(ChatMessage m)
        //    {
        //        foreach (var client in clients)
        //        {
        //            try
        //            {
        //                var data = string.Format("data:{0}|{1}|{2}\n\n", m.username, m.text, m.dt);
        //                await client.WriteAsync(data);
        //                await client.FlushAsync();
        //                client.Dispose();
        //            }
        //            catch (Exception)
        //            {
        //                StreamWriter ignore;
        //                clients.TryTake(out ignore);
        //            }
        //        }
        //    }

        //    [HttpGet]
        //    public HttpResponseMessage Subscribe(HttpRequestMessage request)
        //    {
        //        var response = request.CreateResponse();
        //        response.Content = new PushStreamContent((a, b, c) =>
        //        { OnStreamAvailable(a, b, c); }, "text/event-stream");
        //        return response;
        //    }

        //    private void OnStreamAvailable(Stream stream, HttpContent content,
        //        TransportContext context)
        //    {
        //        var client = new StreamWriter(stream);
        //        clients.Add(client);
        //    }

        //    public class ChatMessage
        //    {
        //        public string username { get; set; }
        //        public string text { get; set; }
        //        public string dt { get; set; }
        //    }
        //// ----------------------------------
    }

}
