﻿using BellEquipment.Common;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace BellEquipment.Controllers
{
    //[RoutePrefix("Report")]
    [Authorize]
    public class ReportController : ApiController
    {
        #region General Populated Values
        //[Route("api/report/models/{id?}")]
        //[Route("api/report/models")]
        [ActionName("Models")]
        [HttpGet()]
        public IHttpActionResult ReportModels(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.Models(Convert.ToInt32(id)));
            }
            else if (!String.IsNullOrEmpty(id) && !Utils.CheckIsNumeric(id))
            {
                return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
            }
            return Ok(Models.ReportModel.ModelsList());
        }

        //[Route("Category/{id?}")]
        [ActionName("Category")]
        [HttpGet()]
        public IHttpActionResult ReportCategory(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.ModelsCategory(Convert.ToInt32(id)));
            }
            else if (!String.IsNullOrEmpty(id) && !Utils.CheckIsNumeric(id))
            {
                return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
            }
            return Ok(Models.ReportModel.ModelsCategoryList());
        }

        //[Route("ModelsCategory/{id?}")]
        [ActionName("ModelsCategory")]
        [HttpGet()]
        public IHttpActionResult ReportModelsCategory(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.ModelsComplete(Convert.ToInt32(id)));
            }
            else if (!String.IsNullOrEmpty(id) && !Utils.CheckIsNumeric(id))
            {
                return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
            }
            return Ok(Models.ReportModel.ModelsCompleteList());
        }

        #region NonStandardAddons
        [HttpGet()]
        [Route("api/report/NonStandardAddons")]
        [Route("api/report/NonStandardAddons/{id:int}")]
        public IHttpActionResult NonStandardAddons(int? id = null)
        {
            if (!string.IsNullOrEmpty(id.ToString()) && Utils.CheckIsNumeric(id.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.NonStandardAddons((int)id);
                return Ok(result);
            }
            return Ok(Models.ReportModel.NonStandardAddons());
        }
        [HttpGet()]
        [Route("api/report/NonStandardAddons/Category/{id:int}")]
        public IHttpActionResult NonStandardAddonsCategory(int id)
        {
            return Ok(Models.ReportModel.NonStandardAddonsByModelCategory(id));
        }
        #endregion

        //[Route("FaultCode/{id?}")]
        [HttpGet()]
        public IHttpActionResult FaultCode(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.FaultCode(Convert.ToInt32(id)));
            }
            else if (!String.IsNullOrEmpty(id) && !Utils.CheckIsNumeric(id))
            {
                return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
            }
            return Ok(Models.ReportModel.FaultCode());
        }
        //[Route("FaultSubCode/{id?}")]
        [HttpGet()]
        public IHttpActionResult FaultSubCode(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.FaultSubCode(Convert.ToInt32(id)));
            }
            else if (!String.IsNullOrEmpty(id) && !Utils.CheckIsNumeric(id))
            {
                return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
            }
            return Ok(Models.ReportModel.FaultSubCode());
        }
        [HttpGet()]
        public IHttpActionResult FaultSubCodeById(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.FaultSubCodeById(Convert.ToInt32(id)));
            }
            else if (!String.IsNullOrEmpty(id) && !Utils.CheckIsNumeric(id))
            {
                return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
            }
            return Ok(Models.ReportModel.FaultSubCode());
        }

        [HttpGet()]
        public IHttpActionResult FaultList()
        {
            Models.BellDbObject.dbResult result = Models.ReportModel.FaultCompleteList();
            return Ok(result);
        }

        //[Route("Application/{id?}")]
        [HttpGet()]
        public IHttpActionResult Application(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.Application(Convert.ToInt32(id)));
            }
            else if (!String.IsNullOrEmpty(id) && !Utils.CheckIsNumeric(id))
            {
                return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
            }
            return Ok(Models.ReportModel.ApplicationList());
        }

        //[Route("Complaints/{id?}")]
        [HttpGet()]
        public IHttpActionResult Complaints(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.Complaint(Convert.ToInt32(id)));
            }
            else if (!String.IsNullOrEmpty(id) && !Utils.CheckIsNumeric(id))
            {
                return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
            }
            return Ok(Models.ReportModel.ComplaintList());
        }

        //[Route("Defects/{id?}")]
        [HttpGet()]
        public IHttpActionResult Defects(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
            {
                return Ok(Models.ReportModel.Defect(Convert.ToInt32(id)));
            }
            else if (!String.IsNullOrEmpty(id) && !Utils.CheckIsNumeric(id))
            {
                return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
            }
            return Ok(Models.ReportModel.DefectList());
        }

        [HttpGet()]
        [Route("api/report/Status")]
        [Route("api/report/Status/{id:int}")]
        [Route("api/report/Status/{StatusName}")]
        public IHttpActionResult Status(int? id = null, string StatusName = null)
        {
            if (!string.IsNullOrEmpty(id.ToString()) && Utils.CheckIsNumeric(id.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportStatus((int)id);
                return Ok(result);
            }
            else if (StatusName != null && !string.IsNullOrEmpty(StatusName.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportStatus(StatusName);
                return Ok(result);
            }
            return Ok(Models.ReportModel.ReportStatus());
        }

        [HttpGet()]
        public IHttpActionResult ReportType(string id = null)
        {
            if (!String.IsNullOrEmpty(id) && Utils.CheckIsNumeric(id))
                return Ok(Models.ReportModel.ReportType(Convert.ToInt32(id)));
            return Ok(Models.ReportModel.ReportType());
        }
        #endregion

        #region Report Management
        [HttpGet()]
        public IHttpActionResult ReportList()
        {
            bool sortListDesc = false;
            if (HttpContext.Current.Request.QueryString != null && HttpContext.Current.Request.QueryString.Count > 0)
                if (HttpContext.Current.Request.QueryString["orderby"] != null)
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["orderby"]) && HttpContext.Current.Request.QueryString["orderby"].Contains("desc"))
                        sortListDesc = true;
            Models.BellDbObject.dbResult result = Models.ReportModel.ReportList(sortListDesc);
            return Ok(result);
        }

        [Route("api/Report/Report/{id:int}")]
        [Route("api/Report/Report/{ReportNumber}")]
        [HttpGet()]
        public IHttpActionResult Report(int? id = null, string ReportNumber = null)
        {
            if (!string.IsNullOrEmpty(id.ToString()) && Utils.CheckIsNumeric(id.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.Report(Convert.ToInt32(id));
                return Ok(result);
            }
            else if (ReportNumber != null && !string.IsNullOrEmpty(ReportNumber.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportFind(ReportNumber);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpGet()]
        public IHttpActionResult ReportByUser(string Id)
        {
            if (!string.IsNullOrEmpty(Id.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportByUser(Id);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpPut()]
        public IHttpActionResult ReportSave([System.Web.Http.FromBody] Models.BellDbObject.Report rpt)
        {
            //if (ReportData != null && !string.IsNullOrEmpty(ReportData.ToString()) && ReportData.Customer != null)
            if (Models.ValidationModel.ValidReportDataObject(rpt))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportSave(rpt, User.Identity.GetUserId(), User.Identity.Name);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }
        [HttpPut()]
        public IHttpActionResult GeneratePdf([System.Web.Http.FromBody] Models.BellDbObject.Report rpt)
        {
            //if (ReportData != null && !string.IsNullOrEmpty(ReportData.ToString()) && ReportData.Customer != null)
            if (rpt != null && rpt.ID != null && rpt.ReportPersonId != null)
            {
                var controller = new HomeController();
                RouteData route = new RouteData();
                route.Values.Add("action", "About"); // ActionName
                route.Values.Add("controller", "Home"); // Controller Name
                System.Web.Mvc.ControllerContext newContext = new System.Web.Mvc.ControllerContext(new HttpContextWrapper(System.Web.HttpContext.Current), route, controller);
                controller.ControllerContext = newContext;
                var result = Models.ReportModel.ReportGenerateEmail(controller.ControllerContext, rpt.ID.ToString());
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpPut()]
        public IHttpActionResult ReportSubmit([System.Web.Http.FromBody] Models.BellDbObject.Report rpt)
        {
            //if (ReportData != null && !string.IsNullOrEmpty(ReportData.ToString()) && ReportData.Customer != null)
            if (rpt.ID != null && Models.ValidationModel.ValidReportDataObject(rpt))
            {
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["bellDBConnectionString"].ConnectionString.ToString());
                Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con);
                if (rpt.ID != null && dc.Reports.Where(r => r.ID == rpt.ID).Count() > 0)
                {
                    Models.BellDbObject.dbResult result = Models.ReportModel.ReportSubmit(
                            Models.MapperModel.MapToReportObjectModel(rpt, (from x in dc.Reports where x.ID == rpt.ID select x).FirstOrDefault())
                        , User.Identity.GetUserId());
                    return Ok(result);
                }
                else
                {
                    Models.BellDbObject.dbResult result = Models.ReportModel.ReportSubmit(rpt, User.Identity.GetUserId());
                    return Ok(result);
                }
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpPut()]
        public IHttpActionResult ReportClosed([System.Web.Http.FromBody] Models.BellDbObject.Report rpt)
        {
            //if (ReportData != null && !string.IsNullOrEmpty(ReportData.ToString()) && ReportData.Customer != null)
            if (rpt.ID != null && Models.ValidationModel.ValidReportDataObject(rpt))
            {
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["bellDBConnectionString"].ConnectionString.ToString());
                Models.BellDbObject.BellEquipDataContext dc = new Models.BellDbObject.BellEquipDataContext(con);
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportClosed(
                        Models.MapperModel.MapToReportObjectModel(rpt, (from x in dc.Reports where x.ID == rpt.ID select x).FirstOrDefault())
                    , User.Identity.GetUserId());
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpPut()]
        public IHttpActionResult ReportDuplicate([System.Web.Http.FromBody] int  ReportID)
        {
            if (ReportID > 0)
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportDuplicate(ReportID);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }
        [HttpPut()]
        public IHttpActionResult DuplicateReportFromModel([System.Web.Http.FromBody] Models.BellDbObject.Report rpt)
        {
            if (rpt.ID > 0 && !string.IsNullOrEmpty(rpt.VinNo) && rpt.Hours > 0)
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportDuplicateFromModel((int)rpt.ID, rpt.VinNo, (decimal)rpt.Hours);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }
        #endregion
        #region Report Search By Status
        [Route("api/Report/ReportByStatus/{id:int}")]
        [Route("api/Report/ReportByStatus/{StatusName}")]
        [HttpGet()]
        public IHttpActionResult ReportByStatus(int? id = null, string StatusName = null)
        {
            if (!string.IsNullOrEmpty(id.ToString()) && Utils.CheckIsNumeric(id.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportByStatus((int)id);
                return Ok(result);
            }
            else if (StatusName != null && !string.IsNullOrEmpty(StatusName.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.ReportByStatus(StatusName);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }
        #endregion

        #region Attachments / Documents
        [HttpGet()]
        [Route("api/Document/Content/{id:int}")]
        public IHttpActionResult ReportDocument(int? id = null)
        {
            if (!string.IsNullOrEmpty(id.ToString()) && Utils.CheckIsNumeric(id.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.DocumentsById((int)id);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpGet()]
        [Route("api/Document/Report/{id:int}")]
        public IHttpActionResult DocumentsByReport(int? id = null)
        {
            if (!string.IsNullOrEmpty(id.ToString()) && Utils.CheckIsNumeric(id.ToString()))
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.DocumentsByReport((int)id);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpPost()]
        [Route("api/Document/Save")]
        public IHttpActionResult DocumentSave()
        {
            if (HttpContext.Current!= null && HttpContext.Current.Request != null && HttpContext.Current.Request.Form != null && HttpContext.Current.Request.Form.Count > 0
                && !string.IsNullOrEmpty(HttpContext.Current.Request.Form["ReportID"]) && Utils.CheckIsNumeric(HttpContext.Current.Request.Form["ReportID"])
                && User.Identity.IsAuthenticated)
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    List<HttpPostedFile> postedFiles = new List<HttpPostedFile>();
                    for (int i = 0; i < HttpContext.Current.Request.Files.Count; i++)
                    {
                        postedFiles.Add(HttpContext.Current.Request.Files[i]);
                    }
                    return Ok(Models.ReportModel.DocumentsSave(Convert.ToInt32(HttpContext.Current.Request.Form["ReportID"]), User.Identity.GetUserId(), postedFiles));
                }
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }
        
        [HttpPost()]
        [Route("api/Document/SaveImage")]
        public IHttpActionResult DocumentsSaveBase64String([System.Web.Http.FromBody] Models.ReportModel.DocListBase64String docList)
        {
            if (docList != null && docList != new Models.ReportModel.DocListBase64String() 
                && docList.ReportId > 0 && docList.Docs.Any())
            {
                return Ok(Models.ReportModel.DocumentsSaveBase64(docList.ReportId, User.Identity.GetUserId(), docList.Docs));
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        [HttpPost()]
        [Route("api/Document/Remove")]
        public IHttpActionResult DocumentRemove([System.Web.Http.FromBody] Models.BellDbObject.ReportDocuments rptDoc)
        {
            if (rptDoc != null && rptDoc != new Models.BellDbObject.ReportDocuments() 
                && !string.IsNullOrEmpty(rptDoc.ReportId.ToString()) && Utils.CheckIsNumeric(rptDoc.ReportId.ToString())
                && !string.IsNullOrEmpty(rptDoc.DocumentId.ToString()) && Utils.CheckIsNumeric(rptDoc.DocumentId.ToString())
                )
            {
                Models.BellDbObject.dbResult result = Models.ReportModel.DocumentRemove(rptDoc.ReportId, rptDoc.DocumentId);
                return Ok(result);
            }
            return Ok(new Models.BellDbObject.dbResult() { success = false, error = Common.ErrorHandler.ValidationError.InvalidParameters.GetDescription() });
        }

        #endregion
    }
}
