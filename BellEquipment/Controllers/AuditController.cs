﻿using System;
using System.Web.Http;
using BellEquipment.Common;

namespace BellEquipment.Controllers
{
    public class AuditController : ApiController
    {
        /*
         * AUDIT Records
         * Lets track anything that a client might complain about
        */

        [Route("api/Audit/")]
        [Route("api/Audit/{id?}")]
        [HttpGet()]
        public IHttpActionResult Audit(string id = null)
        {
            //Models.AuditModel.AuditCreateRecordAsync();
            return Ok();
        }

    }
}
