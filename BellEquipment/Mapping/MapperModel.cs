﻿using BellEquipment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static BellEquipment.Models.BellDbObject;

namespace BellEquipment.Models
{
    public class MapperModel
    {
        public static Report MapToReportModel(
            int? ReportID,
            string ReportNumber,
            DateTime? ReportDate,
            string ReportPersonId,
            int? DealersId,
            string Customer,
            string VinNo,
            int? ModelId,
            int? ReportTypeId,
            string ReportedToId,
            int? FaultCodeId,
            int? FaultSubCodeId,
            int? NonStandardAddonsId,
            string FaultCodeReported,
            string FailedPartNumber,
            string ReplacementPartNumber,
            int? ComplaintId,
            int? DefectId,
            string ComplaintDescription,
            string FailedComponentSN,
            string RootCause,
            string ActionRequired,
            bool? SafetyHard,
            int? ApplicaitonId,
            decimal? Hours)
        {
            Report r = new Report();
            r.ID = ReportID;
            r.ReportNumber = ReportNumber;
            r.ReportDate = (ReportDate != null && Convert.ToDateTime(ReportDate) != DateTime.MinValue) ? Convert.ToDateTime(ReportDate) : DateTime.Now;
            r.ReportPersonId = ReportPersonId;
            r.DealersId = DealersId;
            r.Customer = Customer;
            r.VinNo = VinNo;
            r.ModelId = ModelId;
            r.ReportTypeId = ReportTypeId;
            r.ReportedToId = ReportedToId;
            r.FaultCodeId = FaultCodeId;
            r.FaultSubCodeId = FaultSubCodeId;
            r.NonStandardAddonsId = NonStandardAddonsId;
            r.FaultCodeReported = FaultCodeReported;
            r.FailedPartNumber = FailedPartNumber;
            r.ReplacementPartNumber = ReplacementPartNumber;
            r.ComplaintId = ComplaintId;
            r.DefectId = DefectId;
            r.ComplaintDescription = ComplaintDescription;
            r.FailedComponentSN = FailedComponentSN;
            r.RootCause = RootCause;
            r.ActionRequired = ActionRequired;
            r.SafetyHard = SafetyHard;
            r.ApplicaitonId = ApplicaitonId;
            r.Hours = Hours;

            return r;
        }

        public static Report MapToReportObjectModel(Report newReportObject, Report existingReportObj)
        {
            // Match and changed an non default values with new values.
            if (newReportObject.ReportNumber != null && newReportObject.ReportNumber != existingReportObj.ReportNumber)
                existingReportObj.ReportNumber = newReportObject.ReportNumber;
            if (newReportObject.ReportDate != null && newReportObject.ReportDate != existingReportObj.ReportDate)
                existingReportObj.ReportDate = (newReportObject.ReportDate != null && Convert.ToDateTime(newReportObject.ReportDate) != DateTime.MinValue) ? Convert.ToDateTime(newReportObject.ReportDate) : DateTime.Now;
            if (newReportObject.ReportPersonId != existingReportObj.ReportPersonId)
                existingReportObj.ReportPersonId = newReportObject.ReportPersonId;
            if (newReportObject.DealersId != existingReportObj.DealersId)
                existingReportObj.DealersId = newReportObject.DealersId;
            if (newReportObject.Customer != existingReportObj.Customer)
                existingReportObj.Customer = newReportObject.Customer;
            if (newReportObject.VinNo != null && newReportObject.VinNo != existingReportObj.VinNo)
                existingReportObj.VinNo = newReportObject.VinNo;
            if (newReportObject.ModelId != existingReportObj.ModelId)
                existingReportObj.ModelId = newReportObject.ModelId;
            if (newReportObject.ReportTypeId != existingReportObj.ReportTypeId)
                existingReportObj.ReportTypeId = newReportObject.ReportTypeId;
            if (newReportObject.ReportedToId != existingReportObj.ReportedToId)
                existingReportObj.ReportedToId = newReportObject.ReportedToId;
            if (newReportObject.FaultCodeId != null && newReportObject.FaultCodeId != existingReportObj.FaultCodeId)
                existingReportObj.FaultCodeId = newReportObject.FaultCodeId;
            if (newReportObject.FaultSubCodeId != null && newReportObject.FaultSubCodeId != existingReportObj.FaultSubCodeId)
                existingReportObj.FaultSubCodeId = newReportObject.FaultSubCodeId;
            if (newReportObject.NonStandardAddonsId != null && newReportObject.NonStandardAddonsId != existingReportObj.NonStandardAddonsId)
                existingReportObj.NonStandardAddonsId = newReportObject.NonStandardAddonsId;
            if (newReportObject.FaultCodeReported != existingReportObj.FaultCodeReported)
                existingReportObj.FaultCodeReported = newReportObject.FaultCodeReported;
            if (newReportObject.FailedPartNumber != existingReportObj.FailedPartNumber)
                existingReportObj.FailedPartNumber = newReportObject.FailedPartNumber;
            if (newReportObject.ReplacementPartNumber != existingReportObj.ReplacementPartNumber)
                existingReportObj.ReplacementPartNumber = newReportObject.ReplacementPartNumber;
            if (newReportObject.ComplaintId != existingReportObj.ComplaintId)
                existingReportObj.ComplaintId = newReportObject.ComplaintId;
            if (newReportObject.DefectId != existingReportObj.DefectId)
                existingReportObj.DefectId = newReportObject.DefectId;
            if (newReportObject.ComplaintDescription != existingReportObj.ComplaintDescription)
                existingReportObj.ComplaintDescription = newReportObject.ComplaintDescription;
            if (newReportObject.FailedComponentSN != existingReportObj.FailedComponentSN)
                existingReportObj.FailedComponentSN = newReportObject.FailedComponentSN;
            if (newReportObject.RootCause != existingReportObj.RootCause)
                existingReportObj.RootCause = newReportObject.RootCause;
            if (newReportObject.ActionRequired != existingReportObj.ActionRequired)
                existingReportObj.ActionRequired = newReportObject.ActionRequired;
            if (newReportObject.SafetyHard != existingReportObj.SafetyHard)
                existingReportObj.SafetyHard = newReportObject.SafetyHard;
            if (newReportObject.ApplicaitonId != existingReportObj.ApplicaitonId)
                existingReportObj.ApplicaitonId = newReportObject.ApplicaitonId;
            if (newReportObject.Hours != null && newReportObject.Hours != existingReportObj.Hours)
                existingReportObj.Hours = newReportObject.Hours;

            return existingReportObj;
        }
    }

}