
USE BellEquipment

DECLARE @KeyName VARCHAR(100)
DECLARE @KeyValue NVARCHAR(MAX)

SET @KeyName = 'smtpEmailPort'
SET @KeyValue = 587
IF EXISTS (SELECT TOP 1 * FROM [BellEquipment].[dbo].[Configuration] WITH (UPDLOCK,SERIALIZABLE) WHERE Name = @KeyName)
BEGIN
   UPDATE [BellEquipment].[dbo].[Configuration] 
   SET Value = @KeyValue
   WHERE Name = @KeyName
END
ELSE
BEGIN
   INSERT INTO [BellEquipment].[dbo].[Configuration] (Name, Value)
   VALUES (@KeyName, @KeyValue)
END


SET @KeyName = 'smtpEmailHost'
SET @KeyValue = 'smtp.gmail.com'
IF EXISTS (SELECT TOP 1 * FROM [BellEquipment].[dbo].[Configuration] WITH (UPDLOCK,SERIALIZABLE) WHERE Name = @KeyName)
BEGIN
   UPDATE [BellEquipment].[dbo].[Configuration] 
   SET Value = @KeyValue
   WHERE Name = @KeyName
END
ELSE
BEGIN
   INSERT INTO [BellEquipment].[dbo].[Configuration] (Name, Value)
   VALUES (@KeyName, @KeyValue)
END

SET @KeyName = 'smtpEmailUsername'
SET @KeyValue = 'no-reply@mashlab.co.za'
IF EXISTS (SELECT TOP 1 * FROM [BellEquipment].[dbo].[Configuration] WITH (UPDLOCK,SERIALIZABLE) WHERE Name = @KeyName)
BEGIN
   UPDATE [BellEquipment].[dbo].[Configuration] 
   SET Value = @KeyValue
   WHERE Name = @KeyName
END
ELSE
BEGIN
   INSERT INTO [BellEquipment].[dbo].[Configuration] (Name, Value)
   VALUES (@KeyName, @KeyValue)
END

SET @KeyName = 'smtpEmailPassword'
SET @KeyValue = 'Mashlab01!'
IF EXISTS (SELECT TOP 1 * FROM [BellEquipment].[dbo].[Configuration] WITH (UPDLOCK,SERIALIZABLE) WHERE Name = @KeyName)
BEGIN
   UPDATE [BellEquipment].[dbo].[Configuration] 
   SET Value = @KeyValue
   WHERE Name = @KeyName
END
ELSE
BEGIN
   INSERT INTO [BellEquipment].[dbo].[Configuration] (Name, Value)
   VALUES (@KeyName, @KeyValue)
END

SET @KeyName = 'smtpEmailDefaultFromEmail'
SET @KeyValue = 'no-reply@mashlab.co.za'
IF EXISTS (SELECT TOP 1 * FROM [BellEquipment].[dbo].[Configuration] WITH (UPDLOCK,SERIALIZABLE) WHERE Name = @KeyName)
BEGIN
   UPDATE [BellEquipment].[dbo].[Configuration] 
   SET Value = @KeyValue
   WHERE Name = @KeyName
END
ELSE
BEGIN
   INSERT INTO [BellEquipment].[dbo].[Configuration] (Name, Value)
   VALUES (@KeyName, @KeyValue)
END

SET @KeyName = 'ForgotEmailSubject'
SET @KeyValue = 'Your Bell FIR app password has been reset'
IF EXISTS (SELECT TOP 1 * FROM [BellEquipment].[dbo].[Configuration] WITH (UPDLOCK,SERIALIZABLE) WHERE Name = @KeyName)
BEGIN
   UPDATE [BellEquipment].[dbo].[Configuration] 
   SET Value = @KeyValue
   WHERE Name = @KeyName
END
ELSE
BEGIN
   INSERT INTO [BellEquipment].[dbo].[Configuration] (Name, Value)
   VALUES (@KeyName, @KeyValue)
END


SET @KeyName = 'ForgotEmailBody'
SET @KeyValue = '<h2>Bell FIR app password reset</h2>
<h4>Dear {0}</h4>
<div>
	<p>Please log in to the app using your username and your one-time password.</p>
    <p style="border: 1px solid black; padding: 15px; width:300px;">{1}</p>
    <p>Regards,<p/>
    <p>The Bell App Team<p/>
</div>'
IF EXISTS (SELECT TOP 1 * FROM [BellEquipment].[dbo].[Configuration] WITH (UPDLOCK,SERIALIZABLE) WHERE Name = @KeyName)
BEGIN
   UPDATE [BellEquipment].[dbo].[Configuration] 
   SET Value = @KeyValue
   WHERE Name = @KeyName
END
ELSE
BEGIN
   INSERT INTO [BellEquipment].[dbo].[Configuration] (Name, Value)
   VALUES (@KeyName, @KeyValue)
END



