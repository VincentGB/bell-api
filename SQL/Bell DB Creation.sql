/*
DROP DATABASE IF EXISTS BellEquipment;
CREATE DATABASE BellEquipment;
*/
USE BellEquipment

/* LINK TABLE Creation */

DROP TABLE IF EXISTS BellEquipment.dbo.Configuration
DROP TABLE IF EXISTS BellEquipment.dbo.ReportMessages
DROP TABLE IF EXISTS BellEquipment.dbo.Notification
DROP TABLE IF EXISTS BellEquipment.dbo.MessageDocuments
DROP TABLE IF EXISTS BellEquipment.dbo.Messages
DROP TABLE IF EXISTS BellEquipment.dbo.ReportDocuments
DROP TABLE IF EXISTS BellEquipment.dbo.Report
DROP TABLE IF EXISTS BellEquipment.dbo.ReportStatus
DROP TABLE IF EXISTS BellEquipment.dbo.Document
DROP TABLE IF EXISTS BellEquipment.dbo.DealerBranch
DROP TABLE IF EXISTS BellEquipment.dbo.UserBranch
DROP TABLE IF EXISTS BellEquipment.dbo.UserRoles
DROP TABLE IF EXISTS BellEquipment.dbo.Users
DROP TABLE IF EXISTS BellEquipment.dbo.Roles
DROP TABLE IF EXISTS BellEquipment.dbo.Branch
DROP TABLE IF EXISTS BellEquipment.dbo.Notification
DROP TABLE IF EXISTS BellEquipment.dbo.Dealers
DROP TABLE IF EXISTS BellEquipment.dbo.Models
DROP TABLE IF EXISTS BellEquipment.dbo.NonStandardAddons
DROP TABLE IF EXISTS BellEquipment.dbo.ModelsCategory
DROP TABLE IF EXISTS BellEquipment.dbo.ReportType
DROP TABLE IF EXISTS BellEquipment.dbo.FaultSubCode
DROP TABLE IF EXISTS BellEquipment.dbo.FaultCode
DROP TABLE IF EXISTS BellEquipment.dbo.Complaint
DROP TABLE IF EXISTS BellEquipment.dbo.Defect
DROP TABLE IF EXISTS BellEquipment.dbo.Application
DROP TABLE IF EXISTS BellEquipment.dbo.Audit

CREATE TABLE BellEquipment.[dbo].Configuration
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(Max) NULL,
	Value VARCHAR(MAX) NULL,
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)
INSERT INTO BellEquipment.dbo.Configuration (Name, Value) VALUES 
('WebApi-SMTP-URL', ''),
('WebApi-Attachment-Limit', 10),
('WebApi-Attachment-Max-Size', 10000)

/*
CREATE TABLE BellEquipment.[dbo].[Notification]
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(Max) NULL,
	----------
	--CreatedBy VARCHAR(255) NOT NULL DEFAULT 'System',
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.Users (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(100) NOT NULL,
	Surname VARCHAR(100) NOT NULL,
	MobileNumber VARCHAR(50) NOT NULL,
	Email VARCHAR(100) NOT NULL,
	AcountActive BIT DEFAULT 1, 
	----------
	CreatedBy VARCHAR(255) NOT NULL DEFAULT 'System',
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)
*/

CREATE TABLE BellEquipment.dbo.Roles (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(100) NOT NULL,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.UserRoles (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	UserId nvarchar(128) FOREIGN KEY REFERENCES AspNetUsers(Id) NOT Null,
	RoleId INT FOREIGN KEY REFERENCES Roles(Id) NOT Null,
	Status BIT DEFAULT 1, 
	----------
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.Dealers (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(100) NOT NULL,
	MobileNumber VARCHAR(50),
	Email VARCHAR(100),
	Status BIT DEFAULT 1 NOT NULL, 
	----------
	--CreatedBy VARCHAR(255) NOT NULL DEFAULT 'System',
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.Branch (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(250) NOT NULL,
	Status BIT DEFAULT 1 NOT NULL, 
	----------
	--CreatedBy VARCHAR(255) NOT NULL DEFAULT 'System',
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.UserBranch (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	UserId nvarchar(128) FOREIGN KEY REFERENCES AspNetUsers(Id) NOT Null,
	BranchId INT FOREIGN KEY REFERENCES Branch(Id) NOT Null,
	Status BIT DEFAULT 1, 
	----------
)
CREATE TABLE BellEquipment.dbo.DealerBranch (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	DealersId INT FOREIGN KEY REFERENCES Dealers(Id) NOT Null,
	BranchId INT FOREIGN KEY REFERENCES Branch(Id) NOT Null,
	Status BIT DEFAULT 1, 
	----------
)

CREATE TABLE BellEquipment.dbo.ModelsCategory (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)
CREATE TABLE BellEquipment.dbo.Models (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	--Description VARCHAR(MAX) NULL,
	ModelsCategoryId INT FOREIGN KEY REFERENCES ModelsCategory(Id) Null,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.NonStandardAddons (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	ModelsCategoryId INT FOREIGN KEY REFERENCES ModelsCategory(Id) Null,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.ReportType (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.FaultCode (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.FaultSubCode (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	FaultCodeId INT FOREIGN KEY REFERENCES FaultCode(Id) Null,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.Complaint (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.Defect (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.Application (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.ReportStatus (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Title VARCHAR(100) NOT NULL,
	Status BIT DEFAULT 1, 
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

--- Report ---
--------------------------------------------------------------
CREATE TABLE BellEquipment.dbo.Report (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	ReportNumber VARCHAR(100) NULL,
	ReportDate DATETIME NULL,
	ReportPersonId nvarchar(128) FOREIGN KEY REFERENCES AspNetUsers(Id) Null,
	DealersId INT FOREIGN KEY REFERENCES Dealers(Id) Null,
	Customer VARCHAR(250) NULL,
	VinNo VARCHAR(100) NULL,
	ModelId INT FOREIGN KEY REFERENCES Models(Id) Null,
	ReportTypeId INT FOREIGN KEY REFERENCES ReportType(Id) Null,
	ReportedToId nvarchar(128) FOREIGN KEY REFERENCES AspNetUsers(Id) Null, -- ???
	FaultCodeId INT FOREIGN KEY REFERENCES FaultCode(Id) Null,
	FaultSubCodeId INT FOREIGN KEY REFERENCES FaultSubCode(Id) Null,
	NonStandardAddonsId INT FOREIGN KEY REFERENCES NonStandardAddons(Id) Null,
	FaultCodeReported VARCHAR(250) NULL,
	FailedPartNumber VARCHAR(250) NULL,
	ReplacementPartNumber VARCHAR(250) NULL,
	ComplaintId INT FOREIGN KEY REFERENCES Complaint(Id) Null,
	DefectId INT FOREIGN KEY REFERENCES Defect(Id) Null,
	ComplaintDescription VARCHAR(MAX) NULL, -- ???
	FailedComponentSN VARCHAR(100) NULL,
	RootCause VARCHAR(MAX) NULL,
	ActionRequired VARCHAR(MAX) NULL,
	SafetyHard Bit Null,
	ApplicaitonId INT FOREIGN KEY REFERENCES Application(Id) Null,
	Hours DECIMAL(18,2) NULL,
	Status INT FOREIGN KEY REFERENCES ReportStatus(Id) Null DEFAULT 1,
	----------
	--CreatedBy VARCHAR(255) NULL DEFAULT 'System',
	CreatedBy nvarchar(128) FOREIGN KEY REFERENCES AspNetUsers(Id) Null,

	CreatedDate DATETIME NULL DEFAULT GETDATE()
)

-- Attachments
/* Attachments (MAX 2G)
-- https://docs.microsoft.com/en-us/sql/relational-databases/blob/compare-options-for-storing-blobs-sql-server?view=sql-server-2017
-- https://www.jitendrazaa.com/blog/sql/sqlserver/export-documents-saved-as-blob-binary-from-sql-server/
*/
CREATE TABLE BellEquipment.dbo.Document(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Extension [varchar](50) NULL,
	FileName [varchar](200) NULL,
	DocumentContent NVARCHAR(max) NULL,
	----------
	CreatedBy VARCHAR(255) NOT NULL DEFAULT 'System',
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

-- Document and Report Linking Table
CREATE TABLE BellEquipment.dbo.ReportDocuments(
    ID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	ReportId int FOREIGN KEY REFERENCES Report(Id),
	DocumentId int FOREIGN KEY REFERENCES Document(Id),
	IsRemoved Bit NULL DEFAULT 0
)

CREATE TABLE BellEquipment.dbo.Messages (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	Post VARCHAR(MAX) NOT NULL,
	IsDocument Bit NULL DEFAULT 0,
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.ReportMessages (
	ID INT IDENTITY(1,1) PRIMARY KEY,
	ReportId INT FOREIGN KEY REFERENCES Report(Id) NOT Null,
	UserId nvarchar(128) FOREIGN KEY REFERENCES AspNetUsers(Id) NOT Null,
	MessageId INT FOREIGN KEY REFERENCES Messages(Id) NOT Null,
)

-- Document and essages Linking Table
CREATE TABLE BellEquipment.dbo.MessageDocuments(
    ID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	MessageId int FOREIGN KEY REFERENCES Messages(Id),
	DocumentId int FOREIGN KEY REFERENCES Document(Id),
	IsRemoved Bit NULL DEFAULT 0
)

CREATE TABLE BellEquipment.dbo.Notification(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	ReportId INT FOREIGN KEY REFERENCES Report(Id) NOT Null,
	ActionType VARCHAR(100) NULL,
	ActionId INT NULL,
	CreatedBy VARCHAR(255) NOT NULL DEFAULT 'System',
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)

CREATE TABLE BellEquipment.dbo.Audit(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	UserId nvarchar(128) FOREIGN KEY REFERENCES AspNetUsers(Id) NOT Null,
	ActionType VARCHAR(100) NULL,
	Payload VARCHAR(MAX) NOT NULL,
	CreatedDate DATETIME NOT NULL DEFAULT GETDATE()
)


/*

SELECT TOP 100 * FROM BellEquipment.dbo.Complaint
SELECT TOP 100 * FROM BellEquipment.dbo.Defect
SELECT TOP 100 * FROM BellEquipment.dbo.Application

SELECT TOP 1000 * FROM BellEquipment.dbo.Models m LEFT JOIN BellEquipment.dbo.ModelsCategory c ON m.ModelsCategoryId = c.Id ORDER BY c.id
-- SELECT TOP 100 * FROM BellEquipment.dbo.ModelsCategory
-- SELECT TOP 100 * FROM BellEquipment.dbo.Models

SELECT TOP 1000 fs.*, '***' AS [----], fc.* FROM BellEquipment.dbo.FaultSubCode fs LEFT JOIN BellEquipment.dbo.FaultCode fc ON fs.FaultCodeId = fc.id
-- SELECT TOP 100 * FROM BellEquipment.dbo.FaultSubCode
-- SELECT TOP 100 * FROM BellEquipment.dbo.FaultCode

SELECT TOP 100 * FROM BellEquipment.dbo.Report
SELECT TOP 100 * FROM BellEquipment.dbo.ReportStatus

SELECT TOP 100 u.*, r.* FROM BellEquipment.dbo.Users u 
LEFT JOIN BellEquipment.dbo.UserRoles ur ON u.ID = ur.UserId
LEFT JOIN BellEquipment.dbo.Roles r ON r.ID = ur.RoleId

-- SELECT TOP 100 * FROM BellEquipment.dbo.Users
-- SELECT TOP 100 * FROM BellEquipment.dbo.Dealers
-- SELECT TOP 100 * FROM BellEquipment.dbo.Branch
-- SELECT TOP 100 * FROM BellEquipment.dbo.UserBranch
-- SELECT TOP 100 * FROM BellEquipment.dbo.DealerBranch

SELECT TOP 100 * 
FROM BellEquipment.dbo.Users u 
LEFT JOIN BellEquipment.dbo.UserBranch ub ON ub.UserId = u.Id
LEFT JOIN BellEquipment.dbo.Branch b ON ub.BranchId = b.Id
LEFT JOIN BellEquipment.dbo.DealerBranch db ON b.Id = db.BranchId
LEFT JOIN BellEquipment.dbo.Dealers d ON db.DealersId = d.Id

SELECT TOP 100 * FROM BellEquipment.dbo.Messages
SELECT TOP 100 * FROM BellEquipment.dbo.ReportMessages


*/

--SELECT TOP 100 rm.*, m.* 
--FROM BellEquipment.dbo.Report r 
--LEFT JOIN BellEquipment.dbo.ReportMessages rm ON r.ID = rm.ReportId
--LEFT JOIN BellEquipment.dbo.Messages m ON m.ID = rm.MessageId
--WHERE m.ID IS NOT NULL

